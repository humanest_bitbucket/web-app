import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: ''
    , redirectTo: ''
    , pathMatch: 'full'
  }  
  , {
    path: ''
    , loadChildren: () => import('./views/pre-login/client/client.module').then(m => m.ClientModule)
  }   
  , {
    path: 'client'
    , loadChildren: () => import('./views/post-login/client/client.module').then(m => m.ClientModule)
  }
  , {
    path: 'admin'
    , loadChildren: () => import('./views/post-login/admin/admin.module').then(m => m.AdminModule)
  }
  , {
    path: 'therapist'
    , loadChildren: () => import('./views/post-login/therapist/therapist.module').then(m => m.TherapistModule)
  }
  , {
    path: 'updatepassword'
    , loadChildren: () => import('../app/views/post-login/common/update-password/update-password.module').then(m => m.UpdatePasswordModule)
  }
  , {
    path: 'magiclink'
    , loadChildren: () => import('../app/views/pre-login/magic-link/magic-link.module').then(m => m.MagicLinkModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
