import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-validation-summary',
  templateUrl: './validation-summary.component.html',
  styleUrls: ['./validation-summary.component.scss']
})
export class ValidationSummaryComponent implements OnInit {

  @Input() public serverErrors: string[];

  constructor() { 
    this.serverErrors = [];
  }

  ngOnInit(): void {
  }
}
