import { FormArray, FormControl, FormGroup, ValidationErrors } from "@angular/forms";

export class ValidationHelper {

    public static readonly localValidationErrorMessage = 'Few fields are invalid. Please check the error message below each field and try again'

    public static onlyNumberValidator(control: FormControl) {
        if(control.value) {
            // RFC 2822 compliant regex
            if (!isNaN(control.value)) {
                return null;
            } else {
                return { 'invalidNumberFormat': true };
            }
        }
        else return null;
    }

    public static emailValidator(control: FormControl) {
        if(control.value) {
            // RFC 2822 compliant regex
            if (control.value.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)) {
                return null;
            } else {
                return { 'invalidEmailAddress': true };
            }
        }
        else return null;
    }

    public static urlValidator(control: FormControl) {
        if (control.value) {
            if (control.value.replace('www.', '').match(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,15}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi)) {
                return null;
            }   
            else {
                return { 'invalidUrl': true };
            }
        }
        else return null;
    }

    public static phoneValidator(control: FormControl) {
        if (control.value) {
            let inputValue = control.value + '';
            if (inputValue.match(/(\+?( |-|\.)?\d{1,2}( |-|\.)?)?(\(?\d{3}\)?|\d{3})( |-|\.)?(\d{3}( |-|\.)?\d{4})/g)) {
                return null;
            }
            else {
                return { 'invalidPhoneNumber': true };
            }
        }
        else return null;
    }

    public static positiveNumberValidator(control: FormControl) {
        if(control.value) {
            if (parseFloat(control.value) > 0) {
                return null;
            }
            else {
                return { 'invalidPositiveNumber': true };;
            }
        }
        else return null;
    }

    public static getValidatorErrorMessage(validatorName: string, propertyLabel: string | undefined, isSelectTypeControl: boolean, showShortMessage: boolean, validatorValue?: any): string {
        let validationMessage = isSelectTypeControl ? 'Select' : 'Enter';
        validationMessage = !showShortMessage ? `Please ${validationMessage.toLowerCase()}` : validationMessage;
        let config: { [key: string]: string } = {
            'required': `${propertyLabel} is a mandatory field`,
            'invalidEmailAddress': `${propertyLabel} is invalid`,
            'min': `${propertyLabel} should be greater than or equal to ${validatorValue.min}`,
            'max': `${propertyLabel} should be less than or equal to ${validatorValue.max}`,
            'minlength': `${propertyLabel} should have minimum ${validatorValue.requiredLength} characters`,
            'maxlength': `${propertyLabel} can have maximum ${validatorValue.requiredLength} characters`,
            'invalidPhoneNumber': `${propertyLabel} is invalid`,
            'invalidUrl': `${propertyLabel} is invalid`,
            'invalidPositiveNumber': `${propertyLabel} should be greater than 0`,
            'invalidConfirmPassword': 'Confirm password should be same as password',
            'invalidNumberFormat': `${propertyLabel} can only have digits`
        };
        return config[validatorName];
    }

    public static showFieldsError(formGroup: FormGroup): boolean {
        let showFieldErros: boolean = false;
        Object.keys(formGroup.controls).forEach(key => {

            if(formGroup.controls[key] instanceof FormGroup) {
                showFieldErros = this.showFieldsError(<FormGroup>formGroup.controls[key]);
            }
            else if (formGroup.controls[key] instanceof FormArray) {
                let formArray = <FormArray>formGroup.controls[key];
                formArray.controls.forEach(element => {
                    if(element instanceof FormGroup) {
                        showFieldErros = this.showFieldsError(element);
                    } 
                });
            }
            
            formGroup.controls[key].markAsTouched();
            const controlErrors: ValidationErrors | undefined | null = formGroup.get(key)?.errors;
            if (controlErrors != null) {
                showFieldErros = true;
            }
        });

        return showFieldErros;
    }
}
