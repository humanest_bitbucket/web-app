import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationMessageComponent } from './validation-message.component';
import { ValidationSummaryComponent } from './validation-summary/validation-summary.component';

@NgModule({
  declarations: [ValidationMessageComponent, ValidationSummaryComponent],
  imports: [
    CommonModule
    , FormsModule
    , ReactiveFormsModule
  ]
  , exports: [
    FormsModule
    , ReactiveFormsModule
    , ValidationMessageComponent
    , ValidationSummaryComponent
  ]
})
export class AppFormModule { 
  public static forRoot(): ModuleWithProviders<AppFormModule> {
    return {
      ngModule: AppFormModule
      , providers: []
    }
  }
 }
