import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrationRoutingModule } from './registration-routing.module';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { RegistrationComponent } from './registration.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [RegistrationComponent],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    AppFormModule,
    MatTabsModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class RegistrationModule { }
