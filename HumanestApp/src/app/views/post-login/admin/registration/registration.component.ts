import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  public registrationForm: FormGroup;
  public title = 'Registration';

  public serverErrors: string[];

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly toastrService: ToastrService
    , private readonly loader: LoaderService){ 
    this.registrationForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    this.setUpRegistrationForm();
  }

  setUpRegistrationForm(): void {
    this.registrationForm = this.formBuilder.group({
      fullName: ['', [Validators.required, Validators.maxLength(200)]]
      , emailAddress: ['', [Validators.required, ValidationHelper.emailValidator]]
      , role: ['Therapist']
    });
  }

  setRole(role: string) {
    this.registrationForm.reset();
    this.serverErrors = [];
    this.registrationForm.patchValue({
      role: role
    });
  }

  onSubmit(){
    if(ValidationHelper.showFieldsError(this.registrationForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/account/register"; // create a service & extend base service and perform action
    let body = {
      "email": this.registrationForm.get('emailAddress')?.value,
      "fullName": this.registrationForm.get('fullName')?.value,
      "role": this.registrationForm.get('role')?.value
    };
    
    this.loader.show();
    this.baseService.post(url,body, null, undefined, false).subscribe((data) => {
      this.loader.hide();
      const role = this.registrationForm.get('role')?.value;
      this.toastrService.success(role + " has been successfully added.", "Success");
      this.setRole(role);
    }, (errorResponse) => {
      this.loader.hide();
      if(errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }
}
