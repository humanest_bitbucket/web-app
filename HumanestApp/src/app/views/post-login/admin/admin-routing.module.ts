import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/core/services/auth-guard.service';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: ''
    , component: AdminComponent
    , children: [
      {
        path: ''
        , redirectTo: 'calendar'
        , pathMatch: 'full'
      }
      , {
        path: 'register'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationModule)
      }
      , {
        path: 'event'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('../common/calendar/calendar.module').then(m => m.CalendarModule)
      }
      , {
        path: 'calendar'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('../admin/admin-calendar/admin-calendar.module').then(m => m.AdminCalendarModule)
      }
      , {
        path: 'pendingevents'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('../common/event-approval/event-approval.module').then(m => m.EventApprovalModule)
      }
      , {
        path: 'view-notes'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('../common/view-session-notes/view-session-notes.module').then(m => m.ViewSessionNotesModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class AdminRoutingModule { }
