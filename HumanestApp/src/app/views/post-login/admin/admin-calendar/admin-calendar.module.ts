import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";
import { Routes, RouterModule } from "@angular/router";
import { WeekViewDesktopCalendarModule } from "../../common/week-view-desktop-calendar/week-view-desktop-calendar.module";
import { AdminCalendarRoutingModule } from "./admin-calendar-routing.module";
import { AdminCalendarComponent } from "./admin-calendar.component";

@NgModule({
    declarations: [
      AdminCalendarComponent,
    ],
    imports: [
      AdminCalendarRoutingModule,
      CommonModule,
      WeekViewDesktopCalendarModule,
      MatCheckboxModule,
      MatCardModule,
      MatDividerModule,
      MatButtonModule,
      MatIconModule
    ]
  })
  export class AdminCalendarModule { }
  