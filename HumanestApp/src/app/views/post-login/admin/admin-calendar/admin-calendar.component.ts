import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { CalendarEventItem } from 'src/app/core/models/calendar-event-item';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import * as _ from "underscore";
import * as moment from 'moment';
import * as momenttz from 'moment-timezone';
import { trigger, transition, style, animate } from '@angular/animations';
import { WeekValueDateRangeChange } from '../../common/week-view-mobile-calendar/week-view-mobile-calendar.component';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { eventTypeText } from 'src/app/core/helpers/text-format-helper';
import { EventTypeConstant } from 'src/app/core/enums/event-type.enum';

@Component({
  selector: 'app-admin-calendar',
  templateUrl: './admin-calendar.component.html',
  styleUrls: ['./admin-calendar.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(-30%)'}),
        animate('200ms ease-in', style({transform: 'translateX(0%)'}))
      ])
    ])
  ]
})
export class AdminCalendarComponent implements OnInit {
  public currentDate = new Date();
  public eventData: CalendarEventItem[];
  public updateEventSubject: Subject<any> = new Subject<any>();
  private isAdmin: boolean;

  constructor(
    private readonly loader: LoaderService
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly cdr: ChangeDetectorRef
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService) { }

  ngOnInit() { 
    const userData = this.localOpsService.fetchClaimDetailsFromAccessToken();
    if (userData.role == UserRoleConstant.admin) {
      this.isAdmin = true;
    }
  }

  async getEvents(startDate: Date, endDate: Date) {
    this.loader.show();
    try {
      const response = await this.baseService.get("api/Scheduling/getadmintherapisteventsbytimezone", { 
        startDate: startDate.toISOString(), 
        endDate: endDate.toISOString(),
        timezone: momenttz.tz.guess()
      }).toPromise();
      this.loader.hide();
      this.eventData = response.data;      
      this.eventData = _.map(this.eventData, function(obj, key){
        const eventTitle = this.getEventTypeText(obj);
        return {
          ...obj, 
          id: obj.eventId,
          title: eventTitle,
          name: eventTitle,
          start: obj.dateTime
        } 
      }.bind(this));
      this.updateEventSubject.next(this.eventData);

    } 
    catch(errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest && 
        errorResponse.error[0] == "No Events are available.") {
          this.eventData = [];
      } else {
        this.toastrService.error(errorResponse.message);
      }
    }
    finally {
      this.loader.hide();
    }
  }

  async onDateChange(dcEvent: WeekValueDateRangeChange) {
    await this.getEvents(dcEvent.startDate, dcEvent.endDate);
  }

  createEvent() {
    this.router.navigateByUrl((this.isAdmin ? 'admin' : 'therapist') + "/event");
  }
  
  getEventTypeText(event): string {
    if(event.name.startsWith("1:1 Therapy appointment")) {
      if(this.isAdmin) {


        return EventTypeConstant.TherapySession1on1Admin
          .replace("{{therapist}}", event.therapistName)
          .replace("{{client}}", event.clientName);
      } else {
        return eventTypeText(event.eventType, event.clientName);
      }
    }

    return event.name;
  }
}
