import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TherapistRoutingModule } from './therapist-routing.module';
import { TherapistComponent } from './therapist.component';
import { HeaderModule } from '../common/header/header.module';


@NgModule({
  declarations: [TherapistComponent],
  imports: [
    CommonModule,
    TherapistRoutingModule,
    HeaderModule
  ]
})
export class TherapistModule { }
