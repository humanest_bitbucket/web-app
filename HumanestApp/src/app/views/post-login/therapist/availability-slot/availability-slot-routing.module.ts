import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AvailabilitySlotComponent } from './availability-slot.component';

const routes: Routes = [{
  path: ''
    , component: AvailabilitySlotComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvailabilitySlotRoutingModule { }
