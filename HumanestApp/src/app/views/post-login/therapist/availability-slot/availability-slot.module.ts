import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AvailabilitySlotRoutingModule } from './availability-slot-routing.module';
import { AvailabilitySlotComponent } from './availability-slot.component';
import { MatSelectModule } from '@angular/material/select';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';


@NgModule({
  declarations: [
    AvailabilitySlotComponent
  ],
  imports: [
    CommonModule,
    AvailabilitySlotRoutingModule,
    AppFormModule,
    MatSelectModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,    
    MatIconModule,
    MatTooltipModule
  ]
})
export class AvailabilitySlotModule { }
