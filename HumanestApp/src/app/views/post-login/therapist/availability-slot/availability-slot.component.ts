import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { TimeSlotMapping, TimeSlotType } from 'src/app/core/enums/time-slot.enum';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';

@Component({
  selector: 'app-availability-slot',
  templateUrl: './availability-slot.component.html',
  styleUrls: ['./availability-slot.component.scss']
})
export class AvailabilitySlotComponent implements OnInit {
  public availability: Array<{ id: number, date: string, startTime: string, endTime: string, starttimelist: Array<TimeSlotType>, endtimelist: Array<TimeSlotType> }> = [];
  public tomorrow: string = moment().add(1, 'days').toISOString();
  public monthLimit: string = moment().add(1, 'months').toISOString();
  public startTimeSlots = TimeSlotMapping.timeSlots;
  public endTimeSlots = TimeSlotMapping.timeSlots;
  public availabilitySlotForm: FormGroup;
  public isDateSelected: boolean = false;
  public isSlotSelected: boolean = false;
  public serverErrors: string[];
  private slotStartDate;

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly router: Router
    , private readonly toastrService: ToastrService
    , private readonly loader: LoaderService) {
    this.availabilitySlotForm = new FormGroup({});
    this.availabilitySlotForm = this.formBuilder.group({
      startDate: []
    });
  }

  ngOnInit(): void {
  }

  populateAvailabilitySlotList() {
    this.availability = [];
    this.slotStartDate = moment.utc(this.availabilitySlotForm.get('startDate')?.value);
    this.setDefaultAvailability();
    this.isDateSelected = true;
  }

  setDefaultAvailability() {
    for (let i = 0; i < 7; i++) {
      this.slotStartDate = this.slotStartDate.add(1, 'days');
      this.availability.push({ id: i, date: this.slotStartDate.format("MM-DD-yyyy"), startTime: null, endTime: null, starttimelist: [...this.startTimeSlots], endtimelist: [...this.endTimeSlots] });
    }
  }

  updateEndTimeSlot(e: any, i: any): void {
    let startTimeindex = this.startTimeSlots.findIndex(x => x.id === e.value);
    let endTimeindex = this.startTimeSlots.findIndex(x => x.id === i.endTime);
    let arrayIndex = this.availability.findIndex(x => x.id === i.id);
    this.availability[arrayIndex].startTime = e.source.value;
    this.availability[arrayIndex].endTime = endTimeindex < startTimeindex ? null : this.availability[arrayIndex].endTime;
    this.availability[arrayIndex].endtimelist = [...this.endTimeSlots].splice(startTimeindex + 1, 96);
    this.checkSlots();
  }

  validateEndTime(e: any, i: any): void {
    let arrayIndex = this.availability.findIndex(x => x.id === i.id);
    this.availability[arrayIndex].endTime = e.source.value;
    this.updateSlots(i);
  }

  addSlot(i: any) {
    let id = this.availability.length;
    let arrKeys = this.availability.map(el => el.date);
    let lastIndex = arrKeys.lastIndexOf(i.date);
    let skip = this.endTimeSlots.findIndex(x => x.id === this.availability[lastIndex].endTime);
    this.availability.splice(lastIndex + 1, 0, { id: id, date: i.date, startTime: null, endTime: null, starttimelist: [...this.startTimeSlots].splice(skip + 1, 96), endtimelist: [...this.endTimeSlots] });
  }

  removeSlot(i: any) {
    this.availability.forEach((value, index) => {
      if (value.id == i.id) this.availability.splice(index, 1);
    });
    this.updateSlots(i);
  }

  updateSlots(i: any) {
    let arrKeys = this.availability.map(el => el.date);
    let firstIndex = arrKeys.indexOf(i.date);
    let lastIndex = arrKeys.lastIndexOf(i.date);

    for (let i = firstIndex; i < lastIndex; i++) {
      let skip = this.startTimeSlots.findIndex(x => x.id === this.availability[i].endTime);
      this.availability[i + 1].starttimelist = [...this.startTimeSlots].splice(skip + 1, 96);
    }
    this.checkSlots();
  }

  checkSlots() {
    let filteredData = [];
    this.availability.filter(x => x.startTime != null && x.endTime != null).forEach(x => {
      filteredData.push({ startDateTime: moment(x.date + ' ' + x.startTime).toISOString(), endDateTime: moment(x.date + ' ' + x.endTime).toISOString() });
    });
    if (filteredData.length > 0) {
      this.isSlotSelected = true;
    }
    else {
      this.isSlotSelected = false;
    }
  }

  onSubmit() {
    if (ValidationHelper.showFieldsError(this.availabilitySlotForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/therapist/Therapist/addappointmentslots";
    let filteredData = [];
    this.availability.filter(x => x.startTime != null && x.endTime != null).forEach(x => {
      filteredData.push({ startDateTime: moment(x.date + ' ' + x.startTime).toISOString(), endDateTime: moment(x.date + ' ' + x.endTime).toISOString() });
    });
    this.loader.show();
    this.baseService.post(url, filteredData, null, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("Availability slots has been successfully submitted.", "Success");
      //TODO: redirect to dashboard
      this.router.navigate(["/therapist"]);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

}
