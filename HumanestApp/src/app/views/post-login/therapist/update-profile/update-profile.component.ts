import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { StorageConstant } from 'src/app/core/constants/storage.constant';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { LanguagesList, ListType, SpecialtyTypeList } from 'src/app/core/enums/counselor-filters.enum';
import { AccessToken } from 'src/app/core/models/access-token';
import { APIResponse } from 'src/app/core/models/api-response';
import { TherapistDetailsResponse } from 'src/app/core/models/therapist-data-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';
import * as _ from "underscore";

@Component({
  selector: 'update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent {
  public title = 'Update-Profile';
  public updateProfileForm: FormGroup;
  public serverErrors: string[];
  private userData: AccessToken;
  private therapistDetails;

  public allSpecialties: Array<ListType> = [];
  public filteredSpecialties: Observable<ListType[]>;
  public selectedSpecialties: Array<ListType> = [];

  public allLanguages: Array<ListType> = [];
  public filteredLanguages: Observable<ListType[]>;
  public selectedLanguages: Array<ListType> = [];

  public pronouns: Array<ListType> = [];
  public licenseTypes: Array<ListType> = [];
  public separatorKeysCodes: number[] = [ENTER, COMMA];
  public areFiltersLoaded = false;
  public isUserCounselor = false;

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly loader: LoaderService) {
    this.updateProfileForm = new FormGroup({});
    this.serverErrors = [];
    this.userData = new AccessToken();
    this.updateProfileForm = this.formBuilder.group({
      fullName: [, [Validators.required, Validators.minLength(8)]]
      , state: [, [Validators.required]]
      , bio: [, [Validators.required]]
      , license: [, [Validators.required]]
      , licenseType: [, [Validators.required]]
      , speciality: [, [Validators.required]]
      , languages: [, [Validators.required]]
      , pronoun: [, [Validators.required]]
    });
  }

  async ngOnInit() {
    this.userData = this.localOpsService.fetchClaimDetailsFromAccessToken();
    this.isUserCounselor = this.userData.role === UserRoleConstant.counselor;
    await this.getFilters();
    await this.getTherapistDetails(this.userData.userId);
    this.setUpUpdateProfileForm();
  }

  async getTherapistDetails(id: string) {
    this.serverErrors = [];

    let url = "api/therapist/Therapist/gettherapistdetails";
    this.loader.show();
    try {
      const response = await this.baseService.get(url, { id: id }, undefined, false).toPromise();
      this.therapistDetails = response.data;
    }
    catch (errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    }
    finally {
      this.loader.hide();
    }
  }

  setUpUpdateProfileForm(): void {

    if (this.isUserCounselor) {
      this.updateProfileForm.get('license').clearValidators();
      this.updateProfileForm.get('licenseType').clearValidators();
      let value = this.licenseTypes?.find(x => x.text.toLowerCase() == StorageConstant.mhw);
      this.updateProfileForm.get('licenseType').setValue(value?.id ?? 0);
    }

    this.updateProfileForm.get('fullName').setValue(this.therapistDetails.fullName);
    this.updateProfileForm.get('license').setValue(this.therapistDetails.license);
    this.updateProfileForm.get('state').setValue(this.therapistDetails.state);
    this.updateProfileForm.get('bio').setValue(this.therapistDetails.bio);
    this.updateProfileForm.get('licenseType').setValue(this.therapistDetails.licenseType);
    this.updateProfileForm.get('pronoun').setValue(this.therapistDetails.pronoun);

    this.selectedLanguages = this.therapistDetails.languages.map((x) => { return { id: this.allLanguages.find(z => z.id == x).id, text: this.allLanguages.find(z => z.id == x).text } as ListType });
    this.updateProfileForm.get('languages').setValue(this.selectedLanguages);

    this.selectedSpecialties = this.therapistDetails.specialities.map((x) => { return { id: this.allSpecialties.find(z => z.id == x).id, text: this.allSpecialties.find(z => z.id == x).text } as ListType });
    this.updateProfileForm.get('speciality').setValue(this.selectedSpecialties);

    this.filteredSpecialties = this.updateProfileForm.get('speciality').valueChanges.pipe(
      startWith(''),
      map((specialty: string | null) => this._filter(specialty, "allSpecialties", "selectedSpecialties")),
    );

    this.filteredLanguages = this.updateProfileForm.get('languages').valueChanges.pipe(
      startWith(''),
      map((language: string | null) => this._filter(language, "allLanguages", "selectedLanguages")),
    );
  }

  selected(
    event: MatAutocompleteSelectedEvent,
    formControlName: string,
    selectProp: string,
    inputElem: HTMLInputElement,
    triggerElem?: MatAutocompleteTrigger): void {

    this[selectProp].push({
      id: parseInt(event.option.id),
      text: event.option.value
    });
    inputElem.value = '';
    this.updateProfileForm.get(formControlName).setValue(null);
    this.updateProfileForm.get(formControlName).setErrors(null);

    setTimeout(() => {
      triggerElem.openPanel();
    });
  }

  remove(item: ListType, prop, formControlName): void {
    const index = this[prop].indexOf(item);

    if (index >= 0) {
      this[prop].splice(index, 1);
    } else {
      this.updateProfileForm.get(formControlName).setValue(null);
    }

  }

  validateInput(formControlName: string, prop: string) {
    if (this[prop].length == 0) {
      this.updateProfileForm.get(formControlName).setErrors({ require: !!this[prop].length });
    }
  }

  onSubmit() {
    if (ValidationHelper.showFieldsError(this.updateProfileForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/account/updatetherapistprofile";
    let body = {
      fullName: this.updateProfileForm.get('fullName')?.value,
      state: this.updateProfileForm.get('state')?.value,
      bio: this.updateProfileForm.get('bio')?.value,
      license: this.updateProfileForm.get('license')?.value,
      specialities: this.selectedSpecialties.map(x => x.id),
      languages: this.selectedLanguages.map(x => x.id),
      pronoun: parseInt(this.updateProfileForm.get('pronoun')?.value) ?? 0,
      licenseType: parseInt(this.updateProfileForm.get('licenseType')?.value) ?? 0
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, false).subscribe((data: APIResponse) => {
      this.loader.hide();
      this.toastrService.success("Profile has been successfully updated.", "Success");
      this.updateProfileForm.reset();
      this.localOpsService.saveAccessToken(data.accessToken);
      this.localOpsService.fetchAccessTokenDetailsAsUserDetails();

      //TODO: redirect to dashboard
      this.router.navigate(["/therapist/calendar"]);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  private _filter(value: string, filterProp: string, selectedProp: string): ListType[] {
    const filterValue = value?.toLowerCase();
    const filterWithoutSelection = [];
    this[filterProp].forEach(item => {
      const hasItem = this[selectedProp].filter(x => x.id == item.id);

      if (!hasItem.length) {
        filterWithoutSelection.push(item);
      }
    });


    let finalList;

    if (filterValue) {
      finalList = filterWithoutSelection.filter(specialty => specialty.text.toLowerCase().includes(filterValue));
    } else {
      finalList = filterWithoutSelection;
    }

    return finalList;
  }


  private async getFilters() {
    this.serverErrors = [];
    let url = "api/therapist/Therapist/gettherapistfilters?filters=Speciality&filters=Language&filters=Pronoun&filters=LicenseType";
    this.loader.show();
    try {
      const response = await this.baseService.get(url, null, undefined, false).toPromise();

      this.allSpecialties = _.filter(response.data, (x) => x.filterName == "Speciality").map(x => x.filterValues)[0].map((x) => { return { id: x.id, text: x.speciality } as ListType });
      this.allLanguages = _.filter(response.data, (x) => x.filterName == "Language").map(x => x.filterValues)[0].map((x) => { return { id: x.languageId, text: x.language } as ListType });
      this.pronouns = _.filter(response.data, (x) => x.filterName == "Pronoun").map(x => x.filterValues)[0].map((x) => { return { id: x.id, text: x.pronoun } as ListType });
      this.licenseTypes = _.filter(response.data, (x) => x.filterName == "LicenseType").map(x => x.filterValues)[0].map((x) => { return { id: x.id, text: x.licenseType } as ListType });

      this.areFiltersLoaded = true;
    }
    catch (errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    }
    finally {
      this.loader.hide();
    }
  }
}
