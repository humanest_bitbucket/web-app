import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/core/services/auth-guard.service';
import { TherapistComponent } from './therapist.component';

const routes: Routes = [{
  path: ''
  , component: TherapistComponent
  , children: [
    {
      path: ''
      , redirectTo: 'calendar'
      , pathMatch: 'full'
    }
    , {
      path: 'updateprofile'
      , canActivate: [AuthGuardService]
      , loadChildren: () => import('./update-profile/update-profile.module').then(m => m.UpdateProfileModule)
    }
    , {
      path: 'event'
      , canActivate: [AuthGuardService]
      , loadChildren: () => import('../common/calendar/calendar.module').then(m => m.CalendarModule)
    }
    , {
      path: 'calendar'
      , canActivate: [AuthGuardService]
      , loadChildren: () => import('../admin/admin-calendar/admin-calendar.module').then(m => m.AdminCalendarModule)
    }
    , {
      path: 'pendingevents'
      , canActivate: [AuthGuardService]
      , loadChildren: () => import('../common/event-approval/event-approval.module').then(m => m.EventApprovalModule)
    }
    , {
      path: 'add-notes'
      , canActivate: [AuthGuardService]
      , loadChildren: () => import('../common/add-session-notes/add-session-notes.module').then(m => m.AddSessionNotesModule)
    }
    , {
      path: 'view-notes'
      , canActivate: [AuthGuardService]
      , loadChildren: () => import('../common/view-session-notes/view-session-notes.module').then(m => m.ViewSessionNotesModule)
    }
    , {
      path: 'availability-slots'
      , canActivate: [AuthGuardService]
      , loadChildren: () => import('../therapist/availability-slot/availability-slot.module').then(m => m.AvailabilitySlotModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class TherapistRoutingModule { }
