import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';

@Component({
  selector: 'app-schedule1on1',
  templateUrl: './schedule1on1.component.html',
  styleUrls: ['./schedule1on1.component.scss']
})
export class Schedule1on1Component implements OnInit {
  public title = 'Schedule 1:1 session';
  public schedule1on1Form: FormGroup;
  
  constructor(private readonly formBuilder: FormBuilder
    , private readonly router: Router) {
    this.schedule1on1Form = new FormGroup({});
  }

  ngOnInit(): void {
    this.setEventTypeForm();
  }

  setEventTypeForm(): void {
    this.schedule1on1Form = this.formBuilder.group({
      state: ['', [Validators.required]]
    });
  }

  onConfirm() {
    if (ValidationHelper.showFieldsError(this.schedule1on1Form)) {
      return;
    }
    let navigationExtras: NavigationExtras = {
      queryParams: { 'state': this.schedule1on1Form.get('state')?.value }
    };

    this.router.navigate(['/client/schedulesession'], navigationExtras);
  }

}
