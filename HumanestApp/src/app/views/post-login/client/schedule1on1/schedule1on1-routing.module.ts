import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Schedule1on1Component } from './schedule1on1.component';

const routes: Routes = [{
  path: ''
    , component: Schedule1on1Component
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Schedule1on1RoutingModule { }
