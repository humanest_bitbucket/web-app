import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Schedule1on1RoutingModule } from './schedule1on1-routing.module';
import { Schedule1on1Component } from './schedule1on1.component';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { StateSelectionModule } from '../../common/state-selection/state-selection.module';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [
    Schedule1on1Component,
  ],
  imports: [
    CommonModule,
    Schedule1on1RoutingModule,
    AppFormModule,
    StateSelectionModule,
    MatButtonModule
  ]
})
export class Schedule1on1Module { }
