import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntakeFormComponent } from './intake-form.component';

const routes: Routes = [{
  path: ''
    , component: IntakeFormComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntakeFormRoutingModule { }
