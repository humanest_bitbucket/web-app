import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { GenderConstant } from 'src/app/core/constants/gender-constants';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';

@Component({
  selector: 'app-intake-form',
  templateUrl: './intake-form.component.html',
  styleUrls: ['./intake-form.component.scss']
})
export class IntakeFormComponent implements OnInit {
  public intakeForm: FormGroup;
  public isConfirmation: boolean = false;
  public isCrisisConsent: boolean = false;
  public isAbove18: boolean = false;
  public serverErrors: string[];
  public genders = GenderConstant.genders;
  public intakeData: any;

  constructor(private route: ActivatedRoute
    , private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly router: Router
    , private readonly loader: LoaderService) {
    this.intakeForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    /*if (!this.route.snapshot.queryParams['id']) {
      this.router.navigate(['/client/dashboard']);
    }*/
    this.setUpIntakeForm();
    this.getEventData();
  }

  setUpIntakeForm(): void {
    this.intakeForm = this.formBuilder.group({
      crisisConsent: [, [Validators.requiredTrue]]
      , above18: [, [Validators.requiredTrue]]
      , age: [, [Validators.required]]
      , gender: [, [Validators.required]]
      , address: ['', [Validators.required]]
      , emergencyContactName: ['', [Validators.required]]
      , emergencyContactNumber: ['', [Validators.required, Validators.pattern(/^\+?([0-9\- ]{9,17})$/)]]
      , emergencyContactRelationship: ['', [Validators.required]]
      , sessionWork: ['', [Validators.required]]
    });
  }

  getEventData(): void {
    this.serverErrors = [];
    let url = "api/Scheduling/GetLastSavedEventDetails";

    this.loader.show();
    this.baseService.get(url, null, undefined, false).subscribe((data: any) => {
      this.loader.hide();
      this.intakeForm.controls['age'].setValue(data.data?.age);
      this.intakeForm.controls['gender'].setValue(data.data?.gender);
      this.intakeForm.controls['address'].setValue(data.data?.address);
      this.intakeForm.controls['emergencyContactName'].setValue(data.data?.emergencyContactName);
      this.intakeForm.controls['emergencyContactNumber'].setValue(data.data?.emergencyContactPhoneNumber);
      this.intakeForm.controls['emergencyContactRelationship'].setValue(data.data?.relationship);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  toggleConfirmation(): void{
    if(!this.isConfirmation && ValidationHelper.showFieldsError(this.intakeForm)){
      return;
    }

    this.intakeData = {
      crisisConsent: this.intakeForm.get('crisisConsent')?.value
      , above18: this.intakeForm.get('above18')?.value
      , age: this.intakeForm.get('age')?.value
      , gender: this.intakeForm.get('gender')?.value
      , address: this.intakeForm.get('address')?.value
      , emergencyContactName: this.intakeForm.get('emergencyContactName')?.value
      , emergencyContactNumber: this.intakeForm.get('emergencyContactNumber')?.value
      , emergencyContactRelationship: this.intakeForm.get('emergencyContactRelationship')?.value
      , sessionWork: this.intakeForm.get('sessionWork')?.value
    };

    this.isConfirmation = !this.isConfirmation;
  }

  onSubmit(): void {
    this.serverErrors = [];
    let url = "api/Scheduling/saveclientevent";
    let body = {
      isCrisisConsent: this.intakeForm.get('crisisConsent')?.value
      , isOver18Consent: this.intakeForm.get('above18')?.value
      , age: this.intakeForm.get('age')?.value
      , gender: this.intakeForm.get('gender')?.value
      , address: this.intakeForm.get('address')?.value
      , emergencyContactName: this.intakeForm.get('emergencyContactName')?.value
      , emergencyContactPhoneNumber: this.intakeForm.get('emergencyContactNumber')?.value
      , relationship: this.intakeForm.get('emergencyContactRelationship')?.value
      , workOnThisSession: this.intakeForm.get('sessionWork')?.value
      , eventId: parseInt(this.route.snapshot.queryParams['id']) ?? 0
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, false).subscribe((data: any) => {
      this.loader.hide();
      let navigationExtras: NavigationExtras = {
        queryParams: { 'id': data.data }
      };

      this.router.navigate(["client/confirmation"],navigationExtras);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

}
