import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntakeFormRoutingModule } from './intake-form-routing.module';
import { IntakeFormComponent } from './intake-form.component';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    IntakeFormComponent
  ],
  imports: [
    CommonModule,
    IntakeFormRoutingModule,
    AppFormModule,    
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDividerModule,
    MatIconModule
  ]
})
export class IntakeFormModule { }
