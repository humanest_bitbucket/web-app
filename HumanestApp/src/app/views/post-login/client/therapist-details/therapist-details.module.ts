import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TherapistDetailsRoutingModule } from './therapist-details-routing.module';
import { TherapistDetailsComponent } from './therapist-details.component';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [
    TherapistDetailsComponent
  ],
  imports: [
    CommonModule,
    TherapistDetailsRoutingModule,
    MatButtonModule
  ]
})
export class TherapistDetailsModule { }
