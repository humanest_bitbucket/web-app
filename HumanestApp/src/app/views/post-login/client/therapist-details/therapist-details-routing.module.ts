import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TherapistDetailsComponent } from './therapist-details.component';

const routes: Routes = [{
  path: ''
    , component: TherapistDetailsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TherapistDetailsRoutingModule { }
