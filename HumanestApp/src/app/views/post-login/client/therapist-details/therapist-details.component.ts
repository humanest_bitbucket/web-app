import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { TherapistDetailsResponse } from 'src/app/core/models/therapist-data-response';
import { BaseService } from 'src/app/core/services/base.service';

@Component({
  selector: 'app-therapist-details',
  templateUrl: './therapist-details.component.html',
  styleUrls: ['./therapist-details.component.scss']
})
export class TherapistDetailsComponent implements OnInit {
  public serverErrors: string[] = [];
  public therapistName: string = '';
  public therapistLicense: string = '';
  public therapistVideoUrl: string = '';
  public therapistBio: string = '';

  constructor(private route: ActivatedRoute
    , private readonly router: Router
    , private readonly baseService: BaseService) { }

  ngOnInit(): void {
    if (!this.route.snapshot.queryParams['id']) {
      this.router.navigate(['/client/dashboard']);
    }
    this.getTherapistDetail();
  }

  getTherapistDetail(): void {
    this.serverErrors = [];

    let url = "api/therapist/Therapist/gettherapistdetails";

    this.baseService.get(url, { id: this.route.snapshot.queryParams['id'] }, undefined, false).subscribe((data: TherapistDetailsResponse) => {
      this.therapistName = data.data.fullName;
      this.therapistLicense = data.data.license;
      this.therapistVideoUrl = data.data.videoUrl;
      this.therapistBio = data.data.bio;
    }, (errorResponse) => {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  onBooking(): void {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'id': this.route.snapshot.queryParams['id'] }
    };
    this.router.navigate(['/client/timeslot'], navigationExtras);
  }

}