import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { GroupEventListComponent } from './group-event-list.component';
import { GroupEventListRoutingModule } from './group-event-list-routing.module';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [
    GroupEventListComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    GroupEventListRoutingModule
  ]
})
export class GroupEventListModule { }
