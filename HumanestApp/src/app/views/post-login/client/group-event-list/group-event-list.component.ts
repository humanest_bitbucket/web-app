import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import * as momenttz from 'moment-timezone';
import * as moment from 'moment/moment'
import { GroupEvent } from 'src/app/core/types/event-types';

@Component({
  selector: 'app-group-event-list',
  templateUrl: './group-event-list.component.html',
  styleUrls: ['./group-event-list.component.scss']
})
export class GroupEventListComponent implements OnInit {
  public eventList: GroupEvent[] = [];
  public noRecords = false;

  constructor(private route: ActivatedRoute
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly baseService: BaseService
    , private readonly loader: LoaderService) { }

  async ngOnInit() {
    await this.getGroupEvents();
  }

  public async getGroupEvents() {
    this.loader.show();
    const url = "api/Scheduling/getgroupevents";
    const response = await this.baseService.get(url, { timezone: momenttz.tz.guess() }).toPromise();
    this.loader.hide();

    if(!response.data.length) {
      this.noRecords = true;
    } else {
      this.deriveValues(response.data);
    }
  }

  private deriveValues(data: GroupEvent[]) {
    this.eventList = [];
    data.forEach(event => {
      const eventItem: GroupEvent = {...event};
      eventItem.displayStart = moment(eventItem.startDateTime).format("dddd, h a") + " " + momenttz.tz(eventItem.timeZone).format("z");
      eventItem.duration = moment.duration(moment(eventItem.endDateTime).diff(moment(eventItem.startDateTime))).asHours() + " hours";
      this.eventList.push(eventItem);
    });
  }

}
