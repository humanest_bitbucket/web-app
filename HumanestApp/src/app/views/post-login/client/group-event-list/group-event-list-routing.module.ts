import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupEventListComponent } from './group-event-list.component';

const routes: Routes = [{
  path: ''
    , component: GroupEventListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupEventListRoutingModule { }
