import { isNgTemplate } from '@angular/compiler';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { TimeZoneConstant } from 'src/app/core/constants/timezone-constants';
import { APIResponse } from 'src/app/core/models/api-response';
import { ClientTimeSlotData, SlotData } from 'src/app/core/models/time-slot-response';
import { BaseService } from 'src/app/core/services/base.service';
import * as moment from 'moment-timezone';
import * as _ from 'underscore';
import { checkOverflow, isMobileView } from 'src/app/core/helpers/html-element-helper';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-time-slot-selection',
  templateUrl: './time-slot-selection.component.html',
  styleUrls: ['./time-slot-selection.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(-30%)'}),
        animate('200ms ease-in', style({transform: 'translateX(0%)'}))
      ])
    ])
  ]
})
export class TimeSlotSelectionComponent implements OnInit {

  public serverErrors: string[] = [];
  public timeSlotForm: FormGroup;
  public timeZoneList = TimeZoneConstant.timeZones;
  public slotData: SlotData[] = [];
  public activeSlotData: SlotData[] = [];
  public activeSlot;
  public noSelectedSlotsError = false;
  public checkOverflow = checkOverflow;
  public isMobileView = isMobileView();
  public slotDisplayInfo: any = {};
  public hasNoSlots: boolean = false;
  private therapistId: string;
  private timezoneId: string;
  private count: number = 0;
  
  constructor(private route: ActivatedRoute
    , private readonly router: Router
    , private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly cdr: ChangeDetectorRef) {
    this.timeSlotForm = new FormGroup({});
    this.timezoneId = TimeZoneConstant.timeZones.find(i => i.id === moment.tz.guess()).id;
  }

  ngOnInit(): void {
    this.therapistId = this.route.snapshot.queryParams.id;
    if (!this.therapistId) {
      this.router.navigate(['/client/dashboard']);
    }
    this.setUpTimeSlotForm();
    this.getTimeSlots();
    if(this.isMobileView) {
      this.setSlotsToBeDisplayed();
    }
  }

  setUpTimeSlotForm(): void {
    this.timeSlotForm = this.formBuilder.group({
      eventTime: ['', [Validators.required]]
      // , timeZone: [this.timeZoneList[6].id, [Validators.required]]
    });
  }

  getTimeSlots(): void {
    this.serverErrors = [];
    this.slotData = [];

    let url = "api/Scheduling/GetClientEventTimeSlotsByTimezone";

    this.baseService.get(url, 
      { 
        therapistId: this.route.snapshot.queryParams['id'], 
        timezone: moment.tz.guess() 
      }, undefined, false)
      .subscribe((data: ClientTimeSlotData) => {

        this.slotData = this.convertDate(data.data);

        if(this.isMobileView) {
          this.setActiveSlotData();
        }
        this.cdr.detectChanges();
      }, (errorResponse) => {
        if (errorResponse.status === HttpStatusConstant.badRequest) {
          this.serverErrors = errorResponse.error;

          if(errorResponse.error[0] == "No appointment slots are available.") {
            this.hasNoSlots = true;
          }
        }
      });
  }

  convertDate(data: SlotData[]): SlotData[] {
    data.forEach(item => {
      
      const dateISO = item.date;
      item.displayDate = moment.utc(dateISO).format("MMM DD");

      item.appointmentSlots.forEach(timeObj => {
        const dateISO = timeObj.dateTime;
        const textValue = moment.utc(dateISO).format("hh:mm A");
        timeObj.displayTime = textValue;
      });
    });

    return data;
  }

  selectTimeslot(slot, date) {
    if(this.activeSlot == slot) {
      this.activeSlot = null;
      this.timeSlotForm.get("eventTime").setValue(null);
      this.noSelectedSlotsError = false;
    } else {
      this.activeSlot = slot;
      this.timeSlotForm.get("eventTime").setValue({time: slot, date });
    }
  }

  onSubmit(): void {

    if(!this.activeSlot) {
      this.noSelectedSlotsError = true;
      return;
    }

    this.serverErrors = [];
    let url = "api/Scheduling/saveclientevent";
    const selectedSlot = this.timeSlotForm.get("eventTime").value;

    let body = {
      therapistId: this.therapistId,
      EventDateTime: moment(selectedSlot.time.dateTime).toISOString(),
      timezone: this.timezoneId
    };

    this.baseService.post(url, body, null, undefined, false).subscribe((data: APIResponse) => {
      let navigationExtras: NavigationExtras = {
        queryParams: { 'id': (data as any).data }
      };
      this.router.navigate(['/client/intakeform'], navigationExtras);
    }, (errorResponse) => {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  private setSlotsToBeDisplayed() {
    this.slotDisplayInfo.rowsTobeDisplayed = 3;
    this.slotDisplayInfo.activeIndex = 0;
    this.slotDisplayInfo.hasDataLeft = false;
    this.slotDisplayInfo.hasDataRight = false;
  }

  public setActiveSlotData(index?) {
    this.slotDisplayInfo.activeIndex = index ?? this.slotDisplayInfo.activeIndex;
    this.activeSlotData = this.slotData.slice(this.slotDisplayInfo.activeIndex, this.slotDisplayInfo.activeIndex + this.slotDisplayInfo.rowsTobeDisplayed);
    this.slotDisplayInfo.hasDataLeft = this.slotDisplayInfo.activeIndex > 0;
    this.slotDisplayInfo.hasDataRight = this.slotDisplayInfo.rowsTobeDisplayed + this.slotDisplayInfo.activeIndex < this.slotData.length;
    this.cdr.detectChanges();
  }

}
