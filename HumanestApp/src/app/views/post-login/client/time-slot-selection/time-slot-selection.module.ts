import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimeSlotSelectionRoutingModule } from './time-slot-selection-routing.module';
import { TimeSlotSelectionComponent } from './time-slot-selection.component';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    TimeSlotSelectionComponent
  ],
  imports: [
    CommonModule,
    TimeSlotSelectionRoutingModule,
    AppFormModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class TimeSlotSelectionModule { }
