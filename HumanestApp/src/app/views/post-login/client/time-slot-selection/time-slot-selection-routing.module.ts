import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimeSlotSelectionComponent } from './time-slot-selection.component';

const routes: Routes = [{
  path: ''
    , component: TimeSlotSelectionComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeSlotSelectionRoutingModule { }
