import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent {
  public showHeaderFooter: boolean = true;

  constructor(
    private readonly localOpsService: LocalOpsService,
    private readonly router: Router) {
      this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.updateShowHeader();
      });
   }

   ngOnInit(): void {
    this.updateShowHeader();
  }

  updateShowHeader() {
    if(this.router.url.includes('initialprofile')){
      this.showHeaderFooter = false;
    }
    else{
      this.showHeaderFooter = true;
    }
  }

  public get currentPath(): string {
    return this.router.url.split("/client/")[1];
  }
  
  logout(): void{
    this.localOpsService.clearLocalStorage();
    this.router.navigate(["/login"]);
  }

}
