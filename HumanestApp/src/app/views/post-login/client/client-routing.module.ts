import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/core/services/auth-guard.service';
import { ClientComponent } from './client.component';

const routes: Routes = [
  {
    path: ''
    , component: ClientComponent
    , children: [
      {
        path: ''
        , redirectTo: 'dashboard'
        , pathMatch: 'full'
      }
      , {
        path: 'dashboard'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
      , {
        path: 'initialprofile'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./initial-profile/initial-profile.module').then(m => m.InitialProfileModule)
      }
      , {
        path: 'schedule1on1'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./schedule1on1/schedule1on1.module').then(m => m.Schedule1on1Module)
      }, {
        path: 'calendar'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./client-calendar/client-calendar.module').then(m => m.ClientCalendarModule)
      }
      , {
        path: 'schedulesession'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./schedule-session/schedule-session.module').then(m => m.ScheduleSessionModule)
      }
      , {
        path: 'therapistdetails'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./therapist-details/therapist-details.module').then(m => m.TherapistDetailsModule)
      }      
      , {
        path: 'intakeform'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./intake-form/intake-form.module').then(m => m.IntakeFormModule)
      }
      , {
        path: 'timeslot'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./time-slot-selection/time-slot-selection.module').then(m => m.TimeSlotSelectionModule)
      }
      , {
        path: 'confirmation'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./confirmation/confirmation.module').then(m => m.ConfirmationModule)
      }
      , {
        path: 'searchcounselor'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./search-counselor/search-counselor.module').then(m => m.SearchCounselorModule)
      }
      , {
        path: 'counselorlist'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./counselor-list/counselor-list.module').then(m => m.CounselorListModule)
      }
      , {
        path: 'groupevents'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./group-event-list/group-event-list.module').then(m => m.GroupEventListModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class ClientRoutingModule { }
