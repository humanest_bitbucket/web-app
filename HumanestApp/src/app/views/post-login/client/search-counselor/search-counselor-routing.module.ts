import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchCounselorComponent } from './search-counselor.component';

const routes: Routes = [{
  path: ''
    , component: SearchCounselorComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchCounselorRoutingModule { }
