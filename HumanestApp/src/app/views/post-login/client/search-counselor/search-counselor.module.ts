import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchCounselorRoutingModule } from './search-counselor-routing.module';
import { SearchCounselorComponent } from './search-counselor.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [
    SearchCounselorComponent
  ],
  imports: [
    CommonModule,
    SearchCounselorRoutingModule,
    MatSelectModule,
    MatButtonModule,
    MatFormFieldModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    MatInputModule,
    AppFormModule,    
  ]
})
export class SearchCounselorModule { }
