import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DaysList, ListType } from 'src/app/core/enums/counselor-filters.enum';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { CounselorFilter } from 'src/app/core/models/counselor-filters';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import * as _ from "underscore";

@Component({
  selector: 'app-search-counselor',
  templateUrl: './search-counselor.component.html',
  styleUrls: ['./search-counselor.component.scss']
})
export class SearchCounselorComponent implements OnInit {
  public searchCounselorForm: FormGroup;
  public serverErrors: string[] = [];
  public allSpecialties: Array<ListType> = [];
  public filteredSpecialties: Observable<ListType[]>;
  public selectedSpecialties: Array<ListType> = [];

  public allLanguages: Array<ListType> = [];
  public filteredLanguages: Observable<ListType[]>;
  public selectedLanguages: Array<ListType> = [];

  public pronouns: Array<ListType> = [];

  public availabilities: Array<ListType> = DaysList.daysList;
  public filteredAvailabilities: Observable<ListType[]>;
  public selectedAvailabilities: Array<ListType> = [];
  
  public separatorKeysCodes: number[] = [ENTER, COMMA];
  public areFiltersLoaded = false;


  @ViewChild('specialityInput') specialityInput: ElementRef<HTMLInputElement>;
  @ViewChild('hiddenSpecialtyInput') hiddenSpecialtyInput: ElementRef<HTMLInputElement>;

  constructor(private route: ActivatedRoute
    , private readonly router: Router
    , private readonly formBuilder: FormBuilder
    , private readonly localOpsService: LocalOpsService
    , private readonly baseService: BaseService
    , private readonly loader: LoaderService) {
    this.searchCounselorForm = new FormGroup({});
  }

  async ngOnInit() {
    if (!this.route.snapshot.queryParams['state']) {
      this.router.navigate(['/client/dashboard']);
    }
    await this.getFilters();
    this.setSearchCounselorForm();
  }

  setSearchCounselorForm(): void {
    this.searchCounselorForm = this.formBuilder.group({
      speciality: [],
      languages: [],
      pronouns: [],
      availability: [],
    });

    this.filteredSpecialties = this.searchCounselorForm.get('speciality').valueChanges.pipe(
      startWith(''),
      map((specialty: string | null) => this._filter(specialty, "allSpecialties", "selectedSpecialties")),
    );

    this.filteredLanguages = this.searchCounselorForm.get('languages').valueChanges.pipe(
      startWith(''),
      map((language: string | null) => this._filter(language, "allLanguages", "selectedLanguages")),
    );

    this.filteredAvailabilities = this.searchCounselorForm.get('availability').valueChanges.pipe(
      startWith(''),
      map((availability: string | null) => this._filter(availability, "availabilities", "selectedAvailabilities")),
    );
  }

  onShowResult() {
    let counselorFilter: CounselorFilter = {
      specialty: this.selectedSpecialties.map(x => x.id),
      language: this.selectedLanguages.map(x => x.id),
      dayOfWeek: this.selectedAvailabilities.map(x => x.id),
      pronouns: this.searchCounselorForm.get('pronouns')?.value,
      state: this.route.snapshot.queryParams['state']
    };
    this.localOpsService.saveCounselorFilter(counselorFilter);
    this.router.navigate(["client/counselorlist"]);
  }

  onShowAll() {
    this.localOpsService.clearCounselorFilter();
    this.router.navigate(["client/counselorlist"]);
  }

  remove(item: ListType, prop, formControlName): void {
    const index = this[prop].indexOf(item);

    if (index >= 0) {
      this[prop].splice(index, 1);
    }

    this.searchCounselorForm.get(formControlName).setValue(null);
  }

  selected(
    event: MatAutocompleteSelectedEvent,
    formControlName: string,
    selectProp: string,
    inputElem: HTMLInputElement,
    triggerElem?: MatAutocompleteTrigger): void {

    this[selectProp].push({
      id: parseInt(event.option.id),
      text: event.option.value
    });
    inputElem.value = '';
    this.searchCounselorForm.get(formControlName).setValue(null);

    setTimeout(() => {
      triggerElem.openPanel();
    });
  }

  validateInput(formControlName: string, prop: string) {
    if(this[prop].length == 0) {
      this.searchCounselorForm.get(formControlName).setErrors({require: !!this[prop].length});
    }
  }

  private _filter(value: string, filterProp: string, selectedProp: string): ListType[] {
    const filterValue = value?.toLowerCase();
    const filterWithoutSelection = [];
    this[filterProp].forEach(item => {
      const hasItem = this[selectedProp].filter(x => x.id == item.id);

      if (!hasItem.length) {
        filterWithoutSelection.push(item);
      }
    });


    let finalList;

    if (filterValue) {
      finalList = filterWithoutSelection.filter(specialty => specialty.text.toLowerCase().includes(filterValue));
    } else {
      finalList = filterWithoutSelection;
    }

    return finalList;
  }

  private async getFilters() {
    this.serverErrors = [];
    let url = "api/therapist/Therapist/gettherapistfilters?filters=Speciality&filters=Language&filters=Pronoun";
    this.loader.show();
    try {
      const response = await this.baseService.get(url, null, undefined, false).toPromise();

      this.allSpecialties = _.filter(response.data, (x) => x.filterName == "Speciality").map(x => x.filterValues)[0].map((x) => { return { id: x.id, text: x.speciality } as ListType});
      this.allLanguages = _.filter(response.data, (x) => x.filterName == "Language").map(x => x.filterValues)[0].map((x) => { return { id: x.languageId, text: x.language } as ListType});
      this.pronouns = _.filter(response.data, (x) => x.filterName == "Pronoun").map(x => x.filterValues)[0].map((x) => { return { id: x.id, text: x.pronoun } as ListType});

      this.areFiltersLoaded = true;
    } 
    catch(errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    }
    finally {
      this.loader.hide();
    }
  }
}

