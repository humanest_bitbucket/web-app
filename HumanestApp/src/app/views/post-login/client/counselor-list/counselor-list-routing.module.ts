import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CounselorListComponent } from './counselor-list.component';

const routes: Routes = [{
  path: ''
    , component: CounselorListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CounselorListRoutingModule { }
