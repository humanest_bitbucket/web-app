import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { CounselorFilter } from 'src/app/core/models/counselor-filters';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-counselor-list',
  templateUrl: './counselor-list.component.html',
  styleUrls: ['./counselor-list.component.scss']
})
export class CounselorListComponent implements OnInit {
  public serverErrors: string[] = [];
  public counselorList: Array<{ id: string, therapistName: string, imageUrl: string, licenseType: string }> = [];
  public noRecords = false;

  constructor(private route: ActivatedRoute
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly baseService: BaseService
    , private readonly loader: LoaderService
    , private readonly location: Location) { }

  ngOnInit(): void {
    this.getTherapistDetails();
  }

  getTherapistDetails() {
    this.serverErrors = [];
    let data: CounselorFilter = this.localOpsService.fetchCounselorFilter();

    let url = "api/therapist/Therapist/gettherapists";
    let therapistDTO = {
      specialities: data?.specialty,
      languages: data?.language,
      pronoun: data?.pronouns ?? 0,
      licenseType: data?.licenseType ?? 0,
      daysOfTheWeekAvailability: data?.dayOfWeek,
      state: data?.state
    };

    this.loader.show();
    this.baseService.post(url, therapistDTO, null, undefined, false).subscribe(data => {
      this.loader.hide();
      if(data.data.length) {
        data.data.forEach(item => this.counselorList.push({ id: item.id, therapistName: item.fullName, imageUrl: item.imageUrl, licenseType: item.licenseType }));
      } else {
        this.noRecords = true;
      }
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  navigateTherapistDetail(id: string) {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'id': id }
    };

    this.router.navigate(["client/therapistdetails"], navigationExtras);
  }

  onBooking(id: string) {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'id': id }
    };

    this.router.navigate(["client/timeslot"], navigationExtras);
  }

  goBack() {
    this.location.back();
  }
}
