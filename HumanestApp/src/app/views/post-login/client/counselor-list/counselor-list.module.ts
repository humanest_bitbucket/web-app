import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CounselorListRoutingModule } from './counselor-list-routing.module';
import { CounselorListComponent } from './counselor-list.component';
import { MatButtonModule } from '@angular/material/button';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';


@NgModule({
  declarations: [
    CounselorListComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    CounselorListRoutingModule,
    AppFormModule,
  ]
})
export class CounselorListModule { }
