import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import * as moment from 'moment';
import * as momenttz from 'moment-timezone';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { TherapistDetailsResponse } from 'src/app/core/models/therapist-data-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import "../../../../core/helpers/add-to-calendar.js";
declare var $: any;

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
  public isConfirmed: boolean = false;
  public serverErrors: string[];
  public therapistName: string;
  public eventDate: string;
  public eventDay: string;
  public eventTime: string;
  public eventEndTime: string;
  public eventData: any;
  public eventTimezone: string;
  public eventTimezoneId: string;
  private therapistId: string;

  constructor(private route: ActivatedRoute
    , private readonly router: Router
    , private readonly baseService: BaseService
    , private readonly loader: LoaderService) { }

  ngOnInit(): void {
    this.getEventData();
  }

  ngAfterViewInit() {

  }

  onConfirmation(): void {
    this.serverErrors = [];
    let url = "api/Scheduling/saveclientevent";
    let body = {
      IsSessionConfirmed: true
      , eventId: parseInt(this.route.snapshot.queryParams['id'])
      , therapistId: this.therapistId
      , timezone: this.eventTimezoneId
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, false).subscribe(data => {
      this.loader.hide();
      this.isConfirmed = true;
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  getEventData(): void {
    this.serverErrors = [];
    let url = "api/Scheduling/geteventbyid";

    this.loader.show();
    this.baseService.get(url, { eventId: this.route.snapshot.queryParams['id'] }, undefined, false).subscribe((data: any) => {
      this.loader.hide();
      this.therapistId = data.data.therapistId;
      this.getTherapistDetail();
      
      this.eventData = data.data;
      this.eventDate = momenttz.utc(data.data.startDateTime).tz(data.data.timeZone).format("MM-DD-yyyy");
      this.eventTime = momenttz.utc(data.data.startDateTime).tz(data.data.timeZone).format("hh:mm A");
      this.eventEndTime = momenttz.utc(data.data.endDateTime).tz(data.data.timeZone).format("hh:mm A");
      this.eventTimezone = momenttz().tz(data.data.timeZone).format('z');
      this.eventTimezoneId = data.data.timeZone;
      this.eventDay = moment(this.eventDate).format('dddd');
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  getTherapistDetail(): void {
    this.serverErrors = [];

    let url = "api/therapist/Therapist/gettherapistdetails";

    this.baseService.get(url, { id: this.therapistId }, undefined, false).subscribe((data: TherapistDetailsResponse) => {
      this.therapistName = data.data.fullName;
    }, (errorResponse) => {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

}
