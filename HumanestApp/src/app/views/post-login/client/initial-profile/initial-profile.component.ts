import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { ConsentType } from 'src/app/core/enums/consent.enum';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';

@Component({
  selector: 'app-initial-profile',
  templateUrl: './initial-profile.component.html',
  styleUrls: ['./initial-profile.component.scss']
})
export class InitialProfileComponent {

  public clientProfileForm: FormGroup;
  public isCrisisConsent: boolean = false;
  public serverErrors: string[];
  public today: string = moment().subtract(18, 'years').toISOString();

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly loader: LoaderService) {
    this.clientProfileForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    this.setUpClientProfileForm();
  }

  setUpClientProfileForm(): void {
    this.clientProfileForm = this.formBuilder.group({
      firstName: ['', [Validators.required]]
      , lastName: ['', [Validators.required]]
      , dob: ['', [Validators.required]]
      , crisisConsent: [, [Validators.requiredTrue]]
    });
  }

  onSubmit() {
    if (ValidationHelper.showFieldsError(this.clientProfileForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/account/updateclientprofile";
    let body = {
      firstName: this.clientProfileForm.get('firstName')?.value,
      lastName: this.clientProfileForm.get('lastName')?.value,
      dateOfBirth: this.clientProfileForm.get('dob')?.value,
      isConsentAccepted: this.isCrisisConsent,
      ConsentDetailId: ConsentType.CrisisDisclaimer
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("Profile updated successfully.", "Success");
      this.clientProfileForm.reset();

      this.router.navigate(["client/dashboard"]);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }
}
