import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InitialProfileRoutingModule } from './initial-profile-routing.module';
import { InitialProfileComponent } from './initial-profile.component';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { ToastrModule } from 'ngx-toastr';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MY_FORMATS } from 'src/app/core/constants/DateUtils';

@NgModule({
  declarations: [
    InitialProfileComponent
  ],
  imports: [
    CommonModule,
    InitialProfileRoutingModule,
    AppFormModule,
    ToastrModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class InitialProfileModule { }
