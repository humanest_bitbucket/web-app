import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitialProfileComponent } from './initial-profile.component';

const routes: Routes = [{
  path: ''
    , component: InitialProfileComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitialProfileRoutingModule { }
