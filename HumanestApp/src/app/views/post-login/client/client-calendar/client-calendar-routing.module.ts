import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientCalendarComponent } from './client-calendar.component';


const routes: Routes = [{
    path: '', 
    component: ClientCalendarComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientCalendarRoutingModule { }
