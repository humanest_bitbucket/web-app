import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { CalendarEventItem } from 'src/app/core/models/calendar-event-item';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { WeekValueDateRangeChange } from '../../common/week-view-mobile-calendar/week-view-mobile-calendar.component';
import * as _ from "underscore";
import * as moment from 'moment';
import * as momenttz from 'moment-timezone';
import { trigger, transition, style, animate } from '@angular/animations';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { eventTypeText } from 'src/app/core/helpers/text-format-helper';

@Component({
  selector: 'app-client-calendar',
  templateUrl: './client-calendar.component.html',
  styleUrls: ['./client-calendar.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(-30%)'}),
        animate('200ms ease-in', style({transform: 'translateX(0%)'}))
      ])
    ])
  ]
})
export class ClientCalendarComponent implements OnInit {
  public currentDate = new Date();
  public eventData: CalendarEventItem[];
  public filteredEventData: any[] = [];

  constructor(
    private readonly loader: LoaderService
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly cdr: ChangeDetectorRef) { }

  ngOnInit() { }

  async getEvents(startDate: Date, endDate: Date) {
    this.loader.show();
    try {
      const response = await this.baseService.get("api/Scheduling/getclienteventsbytimezone", { 
        startDate: startDate.toISOString(),
        endDate: endDate.toISOString(),
        timezone: momenttz.tz.guess()
      }).toPromise();
      this.loader.hide();
      this.eventData = response.data;
      
      this.eventData = _.map(this.eventData, function(obj, key){ return {...obj, dateTime: moment(obj.dateTime).toDate()} });
    } 
    catch(errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest && 
        errorResponse.error[0] == "No Events are available.") {
          this.eventData = [];
      } else {
        this.toastrService.error(errorResponse.message);
      }
    }
    finally {
      this.loader.hide();
    }
  }

  async onDateChange(dcEvent: WeekValueDateRangeChange) {
    this.filteredEventData = [];

    await this.getEvents(dcEvent.startDate, dcEvent.endDate);

    const todaysEvent = _.filter(this.eventData, (data: CalendarEventItem) => {
      return data.dateTime.toDateString() == dcEvent.activeDate.toDateString();
    });

    const otherEvents = _.filter(this.eventData, (data: CalendarEventItem) => {
      return data.dateTime.toDateString() != dcEvent.activeDate.toDateString();
    });

    const groupedEvents = _.groupBy(otherEvents, (data: CalendarEventItem) => moment.utc(data.dateTime).format("MM-DD-yyyy"));

    if(todaysEvent.length) {
      this.filteredEventData.push({
        date: dcEvent.activeDate,
        events: todaysEvent
      });
    }

    Object.keys(groupedEvents).forEach((key) => {
      this.filteredEventData.push({
        date: new Date(key),
        events: groupedEvents[key]
      });
    });

    this.cdr.detectChanges();

  }

  isToday(date): boolean {
    const current = moment(date);
    const today = moment();
    return today.isSame(current, 'day');
  }

  getTime(dateTime: Date): string {
    return moment.utc(dateTime, ["HH:mm"]).format("hh:mm A") + " " + moment().tz(momenttz.tz.guess()).format('z');
  }

  subscribeEvent(changeEvent: MatCheckboxChange, eventInfo: CalendarEventItem) {
    const url = "api/Scheduling/" + (changeEvent.checked ? "addattendeetogroupevent": "removeattendeetogroupevent");
    this.baseService.post(url, { eventId: eventInfo.eventId }, null, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success(changeEvent.checked ? this.getSubscribeMessage(eventInfo) : this.getUnsubscribeMessage(eventInfo), "Success");

    }, (errorResponse) => {
      this.loader.hide();
      this.toastrService.error(errorResponse.message);
    });
  }

  private getSubscribeMessage(eventInfo: CalendarEventItem): string {
    return "Successfully subscribed to "+ "' "+ eventInfo.name + " '";
  }

  private getUnsubscribeMessage(eventInfo: CalendarEventItem): string {
    return "Successfully unsubscribed from "+ "' "+ eventInfo.name + " '";
  }

  getEventTypeText(type: number, name: string): string{
    return eventTypeText(type, name);
  }
}
