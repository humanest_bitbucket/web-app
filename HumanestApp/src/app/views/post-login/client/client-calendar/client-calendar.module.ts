import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientCalendarComponent } from './client-calendar.component';
import { WeekViewMobileCalendarModule } from '../../common/week-view-mobile-calendar/week-view-mobile-calendar.module';
import { RouterModule, Routes } from '@angular/router';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { ClientCalendarRoutingModule } from './client-calendar-routing.module';


@NgModule({
  declarations: [ClientCalendarComponent],
  imports: [
    ClientCalendarRoutingModule,
    CommonModule,
    WeekViewMobileCalendarModule,
    MatCheckboxModule,
    MatCardModule,
    MatDividerModule
  ],
  exports:[ClientCalendarComponent]
})
export class ClientCalendarModule { }
