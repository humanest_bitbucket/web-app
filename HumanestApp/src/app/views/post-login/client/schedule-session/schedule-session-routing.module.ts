import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScheduleSessionComponent } from './schedule-session.component';

const routes: Routes = [{
  path: ''
    , component: ScheduleSessionComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduleSessionRoutingModule { }
