import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { APIResponse } from 'src/app/core/models/api-response';
import { TherapistDataResponse } from 'src/app/core/models/therapist-data-response';
import { BaseService } from 'src/app/core/services/base.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';
import * as moment from 'moment-timezone';

interface TherapistCounselor {
  id: string;
  value: string;
}

interface TherapistCounselorList {
  title: string;
  data: TherapistCounselor[];
}

@Component({
  selector: 'app-schedule-session',
  templateUrl: './schedule-session.component.html',
  styleUrls: ['./schedule-session.component.scss']
})
export class ScheduleSessionComponent implements OnInit {
  public scheduleSessionForm: FormGroup;
  public serverErrors: string[] = [];

  constructor(private route: ActivatedRoute
    , private readonly router: Router
    , private readonly formBuilder: FormBuilder
    , private readonly baseService: BaseService) {
    this.scheduleSessionForm = new FormGroup({});
  }

  ngOnInit(): void {
    if (!this.route.snapshot.queryParams['state']) {
      this.router.navigate(['/client/dashboard']);
    }
    this.setScheduleSessionForm();
  }

  setScheduleSessionForm(): void {
    this.scheduleSessionForm = this.formBuilder.group({
      sessionType: [1, [Validators.required]]
    });
  }

  onSubmit(): void {
    if (ValidationHelper.showFieldsError(this.scheduleSessionForm)) {
      return;
    }

    if (parseInt(this.scheduleSessionForm.get('sessionType')?.value) == 1) {
      this.firstAvailableSlot();
    }
    else {
      let navigationExtras: NavigationExtras = {
        queryParams: { 'state': this.route.snapshot.queryParams['state'] }
      };

      this.router.navigate(['/client/searchcounselor'], navigationExtras);
    }
  }

  firstAvailableSlot(): void {
    this.serverErrors = [];

    let url = "api/Scheduling/getfirstavailableappointment";

    this.baseService.get(url, { state: this.route.snapshot.queryParams['state'], timezone: moment.tz.guess() }, undefined, false).subscribe((data: any) => {
      let navigationExtras: NavigationExtras = {
        queryParams: { 'id': data.data }
      };
      this.router.navigate(['/client/intakeform'], navigationExtras);
    }, (errorResponse) => {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }
}
