import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleSessionRoutingModule } from './schedule-session-routing.module';
import { ScheduleSessionComponent } from './schedule-session.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [
    ScheduleSessionComponent
  ],
  imports: [
    CommonModule,
    ScheduleSessionRoutingModule,
    AppFormModule,
    MatRadioModule,
    MatSelectModule,
    MatButtonModule
  ]
})
export class ScheduleSessionModule { }
