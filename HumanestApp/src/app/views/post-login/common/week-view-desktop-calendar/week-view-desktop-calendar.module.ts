import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeekViewDesktopCalendarComponent } from './week-view-desktop-calendar.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import { AdminEventInfoModule } from '../admin-event-info/admin-event-info.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';

FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin,
  listPlugin
])

@NgModule({
  declarations: [WeekViewDesktopCalendarComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatSelectModule,
    FullCalendarModule,
    AdminEventInfoModule,
  ],
  exports: [
    WeekViewDesktopCalendarComponent
  ]
})
export class WeekViewDesktopCalendarModule { }
