import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { CalendarOptions, EventApi, EventClickArg } from '@fullcalendar/core'; 
import * as moment from 'moment';
import { Subject } from 'rxjs';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { CalendarViewsList } from 'src/app/core/enums/calendar-view.enum';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { AdminEventInfoComponent } from '../admin-event-info/admin-event-info.component';
import { WeekValueDateRangeChange } from '../week-view-mobile-calendar/week-view-mobile-calendar.component';

@Component({
  selector: 'week-view-desktop-calendar',
  templateUrl: './week-view-desktop-calendar.component.html',
  styleUrls: ['./week-view-desktop-calendar.component.scss']
})
export class WeekViewDesktopCalendarComponent {

  @Output() dateChange = new EventEmitter<WeekValueDateRangeChange>();
  @ViewChild('fullcalendar') fullcalendar: FullCalendarComponent;
  @Input() updateEventSubject: Subject<any>;
  public calendarOptions: CalendarOptions = {
    headerToolbar: {
      center: 'title'
    },
    timeZone: 'UTC',
    initialView: 'timeGridWeek',
    editable: false,
    selectable: false,
    displayEventTime: false,
    dayMaxEvents: true,
    eventMaxStack: 2,
    moreLinkContent:function(args){
      return '+'+args.num+ (args.num > 1 ? ' events' : ' event');
    },
    eventContent: this.eventRender.bind(this),
    eventClick: this.handleEventClick.bind(this),
    datesSet: this.handleDateSet.bind(this)
  };
  currentEvents: EventApi[] = [];
  public calendarViews = CalendarViewsList.values;
  public isAdmin = false;
  public selectedView;

  constructor(
    public dialog: MatDialog,
    private readonly localOpsService: LocalOpsService) {

    const userData = this.localOpsService.fetchClaimDetailsFromAccessToken();
    if (userData.role == UserRoleConstant.admin) {
      this.isAdmin = true;
    }

    if(window.innerWidth < 1080) {
      this.calendarOptions.initialView = "listWeek";
    } else {
      this.calendarOptions.initialView = this.isAdmin ? "timeGridDay" : "timeGridWeek";
    }
    this.selectedView = this.calendarOptions.initialView;
  }

  ngAfterViewInit() {
    this.updateEventSubject.subscribe((data) => {
      this.currentEvents = data;
      this.calendarOptions.events = data;
    })
    this.emitDateRange();
  }

  public get Calendar() {
    return this.fullcalendar?.getApi();
  }

  handleWeekendsToggle() {
    const { calendarOptions } = this;
    calendarOptions.weekends = !calendarOptions.weekends;
  }

  handleEventClick(clickInfo: EventClickArg) {
    this.dialog.open(AdminEventInfoComponent, {
      data: {
        eventName: clickInfo.event.title,
        showAddNotes: true,
        showActionPlan: true,
        ...clickInfo.event.extendedProps,
      },
    });
  }

  emitDateRange() {

    let calendarApi = this.fullcalendar.getApi();
    
    this.dateChange.emit(
      {
        startDate: calendarApi.view.activeStart,
        endDate: calendarApi.view.activeEnd,
        activeDate: null
      }
    );
  }

  handleDateSet() {
    this.emitDateRange();
  }

  navigate(methodName: string) {
    this.Calendar[methodName]();
  }

  onViewChange(event: MatSelectChange) {
    this.Calendar.changeView(event.value);
  }

  onDateSelect(date: Date) {
    this.Calendar.gotoDate(date);
  }

  eventRender(e) {
    let container = document.createElement('div')
    const eventProps = e.event.extendedProps;
    const isListView = e.view.type == 'listWeek';

    switch(e.eventType) {
      case 1:
        if(this.isAdmin) {
          container.innerHTML = `<div> 
            <div>${isListView ? moment.utc(e.event.startStr).format("hh:mm A") : ""}</div>
            <div>${eventProps.therapistName} (Therapist)</div>
            <div>${eventProps.name}</div>
          </div>`;
        } else {
          container.innerHTML = `<div> 
            <div>${isListView ? moment.utc(e.event.startStr).format("hh:mm A") : ""}</div>
            <div>${eventProps.clientName} (Client)</div>
            <div>${eventProps.name}</div>
          </div>`;
        }
        break;
      default:
        container.innerHTML = `<div> 
            <div>${isListView ? moment.utc(e.event.startStr).format("hh:mm A") : ""}</div>
            <div>${eventProps.therapistName} (Therapist)</div>
            <div>${eventProps.name}</div>
          </div>`;
    }
    
    
    let arrayOfDomNodes = [ container ]
    return { domNodes: arrayOfDomNodes }

  }
}