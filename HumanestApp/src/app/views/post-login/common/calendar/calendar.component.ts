import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as moment from 'moment';
import * as momenttz from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CalendarConstant } from 'src/app/core/constants/calendar.constant';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { TimeZoneConstant } from 'src/app/core/constants/timezone-constants';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { DaysList, DaysListType } from 'src/app/core/enums/day-list.enum';
import { EventType, EventTypeConstant, EventTypeMapping } from 'src/app/core/enums/event-type.enum';
import { RecurringType, RecurringTypeMapping, RepeatEveryType, RepeatEveryTypeMapping } from 'src/app/core/enums/recurring-type.enum';
import { DateTimeHelper } from 'src/app/core/helpers/date-time.helper';
import { eventTypeText } from 'src/app/core/helpers/text-format-helper';
import { AccessToken } from 'src/app/core/models/access-token';
import { ClientDataResponse } from 'src/app/core/models/client-data-response';
import { TherapistDataResponse } from 'src/app/core/models/therapist-data-response';
import { TimeSlotResponse } from 'src/app/core/models/time-slot-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';
import { CustomEventRecurrenceComponent } from '../custom-event-recurrence/custom-event-recurrence.component';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  public title = 'Calendar';
  public eventTypeForm: FormGroup;
  public minDate: Date = new Date();
  public isAdmin: boolean = false;
  public isSession1on1: boolean = true;
  public showRecurringFrequency: boolean = false;
  public showClientList: boolean = false;
  public createZoomLink: boolean = true;
  public eventStartTimeList: Array<{ value: string, text: string, isAvailable: boolean, status?: string }> = [];
  public eventEndTimeList: Array<{ value: string, text: string, isAvailable: boolean, status?: string }> = [];
  public clientList: Array<{ id: string, value: string }> = [];
  public eventTypes = EventTypeMapping.eventTypes;
  public recurringTypes = RecurringTypeMapping.recurringTypes;
  public timeZoneList = TimeZoneConstant.timeZones;
  public therapistList: Array<{ id: string, value: string }> = [];
  public serverErrors: string[];
  public reccurrenceMonthlyDateText = CalendarConstant.monthlyDate;
  public reccurrenceMonthlyWeekdayText = CalendarConstant.monthlyWeekDay;
  public weekCount: number = 0;
  private defaultTimezone = TimeZoneConstant.timeZones.find(i => i.id === momenttz.tz.guess());
  public selectedTimezone = this.defaultTimezone.id;
  public selectedTherapist;
  public selectedClient;
  public filteredOptionsTimezone: Observable<any[]>;
  public filteredOptionsClient: Observable<any[]>;
  public filteredOptionsTherapist: Observable<any[]>;
  private userData = new AccessToken();
  private setDefaultEventDate = moment.utc(this.minDate);

  constructor(private readonly formBuilder: FormBuilder
    , private readonly localOpsService: LocalOpsService
    , private readonly router: Router
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly loader: LoaderService
    , private readonly dialog: MatDialog
    , private readonly location: Location) {
    this.eventTypeForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    this.userData = this.localOpsService.fetchClaimDetailsFromAccessToken();
    if (this.userData.role == UserRoleConstant.admin) {
      this.isAdmin = true;
    }
    this.setEventTypeForm();
    this.getTherapistsList();
    this.getClientsList();
    this.populateMonthlyrecurringList();

    this.setFilters();
  }

  setEventTypeForm(): void {
    this.eventTypeForm = this.formBuilder.group({
      eventType: ['', [Validators.required]],
      client: [''],
      eventName: ['Meeting Invitation - Temporary Event Name', [Validators.required]],
      eventDate: [this.setDefaultEventDate, Validators.required],
      therapist: ['', [Validators.required]],
      eventStartTime: ['', Validators.required],
      eventEndTime: [''],
      recurringType: [RecurringTypeMapping.recurringTypes[0].id],
      recurringGroup: this.formBuilder.group({
        recurringRepeatEveryType: [RepeatEveryTypeMapping.repeatEveryTypes[0].id],
        recurringRepeatEvery: [1],
        recurringRepeatDaysOn: [],
        recurringRepeatMonthly: [this.reccurrenceMonthlyDateText],
        recurringEndsOnDate: [this.setDefaultEventDate],
        recurringEndsOn: ['Never'],
        recurrenceEndsOnOccurrenceCount: [{value: '1', disabled: true}],
      }),      
      timeZone: [this.defaultTimezone.text, [Validators.required]],
      description: ['', [Validators.required]],
      zoomLink: []
    });
  }

  getTherapistsList() {
    this.serverErrors = [];
    this.therapistList = [];
    if (!this.isAdmin) {
      this.therapistList.push({ id: this.userData.userId ?? '', value: this.userData.fullName ?? '' });
      this.eventTypeForm.get('therapist').setValue(this.therapistList[0].value);
      this.selectedTherapist = this.therapistList[0].id;
      this.populateStartTimeList();
      this.setFilters();
    }
    else {
      let url = "api/therapist/Therapist/gettherapists";

      this.loader.show();
      this.baseService.post(url, {}, null, undefined, false).subscribe((data: TherapistDataResponse) => {
        this.loader.hide();
        data.data.forEach(item => this.therapistList.push({ id: item.id, value: item.fullName }));
        this.setFilters();
      }, (errorResponse) => {
        this.loader.hide();
        if (errorResponse.status === HttpStatusConstant.badRequest) {
          this.serverErrors = errorResponse.error;
        }
      });
    }
  }

  changeEventType(e: any): void {
    let selectedIndex = e.source.value;
    if (selectedIndex == EventType.TherapySession1on1) {
      this.showClientList = true;
      this.eventTypeForm.get('client')?.setValidators(Validators.required);
      this.eventTypeForm.get('eventEndTime')?.clearValidators();
      this.isSession1on1 = true;
      this.eventTypeForm.get('recurringType').setValue(RecurringTypeMapping.recurringTypes[0].id);
      this.eventTypeForm.get('recurringGroup').get('recurringRepeatEveryType').setValue(RepeatEveryTypeMapping.repeatEveryTypes[0].id);
      this.eventTypeForm.get('recurringGroup').get('recurringRepeatEvery').setValue(1);
      this.eventTypeForm.get('recurringGroup').get('recurringRepeatDaysOn').setValue(undefined);
      this.eventTypeForm.get('recurringGroup').get('recurringRepeatMonthly').setValue(this.reccurrenceMonthlyDateText);
      this.eventTypeForm.get('recurringGroup').get('recurringEndsOn').setValue('Never');
      this.eventTypeForm.get('recurringGroup').get('recurrenceEndsOnOccurrenceCount').setValue('1');
    }
    else {
      this.showClientList = false;
      this.isSession1on1 = false;
      this.eventTypeForm.get('client')?.clearValidators();
      this.eventTypeForm.get('eventEndTime')?.setValidators(Validators.required);
    }
    if (this.eventTypeForm.get('eventStartTime')?.value) {
      this.populateEndTimeList();
    }
  }

  getClientsList() {
    this.serverErrors = [];
    this.clientList = [];
    let url = "api/client/Client/getclients";

    this.loader.show();
    this.baseService.get(url, null, undefined, false).subscribe((data: ClientDataResponse) => {
      this.loader.hide();
      data.data.forEach(item => {
        let value = item.firstName + ' ' + item.lastName + ' (' + item.email + ')';
        this.clientList.push({ id: item.id, value: value });
      });
      this.setFilters();
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  populateStartTimeList(): void {
    this.serverErrors = [];
    this.eventStartTimeList = [];
    this.populateMonthlyrecurringList();

    let url = "api/Scheduling/geteventtimeslots";
    let body = {
      eventStartDateTime: momenttz.tz(this.eventTypeForm.get('eventDate')?.value, this.selectedTimezone).format(),
      therapistId: this.selectedTherapist,
      isLoadEndTimeSlots: false,
      isAdmin: this.isAdmin,
      timezone: this.selectedTimezone
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, false).subscribe((data: TimeSlotResponse) => {
      
      this.loader.hide();
      data.data.forEach(item => {
        
        const dateISO = item.dateTime + "Z";
        const textValue = moment.utc(dateISO).format("hh:mm A");
        this.eventStartTimeList.push({ 
          value: item.dateTime, 
          text: textValue,
          ...item
        });
      });
      
      this.eventTypeForm.get('eventStartTime')?.setValue(this.eventStartTimeList[0]);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
        this.eventEndTimeList = [];
      }
    });
  }

  populateEndTimeList(): void {
    this.serverErrors = [];
    if (!this.isSession1on1) {
      this.eventEndTimeList = [];
      let url = "api/Scheduling/geteventtimeslots";
      let body = {
        eventStartDateTime: momenttz.tz(this.eventTypeForm.get('eventStartTime')?.value, this.selectedTimezone).toISOString(),
        therapistId: this.selectedTherapist,
        isLoadEndTimeSlots: true,
        isAdmin: this.isAdmin,
        timezone: this.selectedTimezone
      };

      this.loader.show();
      this.baseService.post(url, body, null, undefined, false).subscribe((data: TimeSlotResponse) => {
        this.loader.hide();
        data.data.forEach(item => {
        
          const dateISO = item.dateTime + "Z";
          const textValue = moment.utc(dateISO).format("hh:mm A");
          this.eventEndTimeList.push({ value: item.dateTime, text: textValue, isAvailable: item.isAvailable });
        });
        this.eventTypeForm.get('eventEndTime')?.setValue(this.eventEndTimeList[0]);
      }, (errorResponse) => {
        this.loader.hide();
        if (errorResponse.status === HttpStatusConstant.badRequest) {
          this.serverErrors = errorResponse.error;
        }
      });
    }
  }
  
  updateTimeZoneSelection(option) {
    this.selectedTimezone = option;
    this.eventTypeForm.get("eventStartTime").setValue(null);
    this.eventStartTimeList = [];
    this.populateStartTimeList();
  }

  showCustomDialogue(optionName: string) {
    if(optionName == "Custom") {
      this.openCustomRecurrenceDialogue();
    }
  }

  setFilters() {
    this.filteredOptionsTimezone = this.eventTypeForm.get('timeZone')!.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(this.timeZoneList, "text", value)),
    );
    this.filteredOptionsClient = this.eventTypeForm.get('client')!.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(this.clientList, "value", value)),
    );
    this.filteredOptionsTherapist = this.eventTypeForm.get('therapist')!.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(this.therapistList, "value", value)),
    );
  }

  private _filter(filterList, prop, value: any): TimeZoneConstant[] {
    const filterValue = value[prop] ? value[prop].toLowerCase() : value?.toLowerCase();

    const matchedOptions = filterList.filter(option => option[prop].toLowerCase().includes(filterValue));

    return [...matchedOptions];
  }

  changeZoomLinkCheck(event: any) {
    this.createZoomLink = event.checked;
  }

  changeRecurringFrequency(target: any): void {
    let selectedIndex = target.value;
  }

  openCustomRecurrenceDialogue() {
    this.dialog.open(CustomEventRecurrenceComponent, {
      data: {
        recurringFormGroup: this.eventTypeForm.get('recurringGroup'),
        eventTypeForm: this.eventTypeForm
      }
    });
  }

  populateMonthlyrecurringList() {
    let selectedDate = this.eventTypeForm.get('eventDate')?.value;
    this.reccurrenceMonthlyDateText = CalendarConstant.monthlyDate;
    this.reccurrenceMonthlyWeekdayText = CalendarConstant.monthlyWeekDay;

    let selectedDateValue = new Date(selectedDate);
    let monthStartDay = new Date(selectedDateValue.getFullYear(), selectedDateValue.getMonth(), 1);

    while (true) {
      if (monthStartDay.getDay() == selectedDateValue.getDay()) {
        break;
      }
      monthStartDay.setDate(monthStartDay.getDate() + 1);
    }

    this.weekCount = Math.floor((selectedDateValue.getDate() - monthStartDay.getDate()) / 7);
    this.reccurrenceMonthlyDateText = this.reccurrenceMonthlyDateText + ' ' + selectedDateValue.getDate();
    this.reccurrenceMonthlyWeekdayText = this.reccurrenceMonthlyWeekdayText + ' ' + CalendarConstant.noofWeek[this.weekCount]
      + ' ' + DaysList.daysList[selectedDateValue.getDay()].fullText;
    this.eventTypeForm.get('recurringRepeatMonthly')?.setValue(this.reccurrenceMonthlyDateText);
  }

  setTherapist(value: string) {
    this.selectedTherapist = value;
  }

  setClient(value: string) {
    this.selectedClient = value;
  }

  onSubmit() {
    if (ValidationHelper.showFieldsError(this.eventTypeForm)) {
      return;
    }

    const utcStartDateTime = momenttz.tz(
      this.eventTypeForm.get('eventStartTime')?.value,
      this.selectedTimezone
    ).utc().format();

    const utcEndDateTime = this.eventTypeForm.get('eventEndTime')?.value ? momenttz.tz(
      this.eventTypeForm.get('eventEndTime')?.value,
      this.selectedTimezone
    ).utc().format() : "";

    let url = "api/Scheduling/saveevent";
    let body = {
      name: this.eventTypeForm.get('eventName')?.value,
      therapistId: this.selectedTherapist,
      type: parseInt(this.eventTypeForm.get('eventType')?.value),
      clientId: this.selectedClient,
      StartDateTime: utcStartDateTime,
      isRecurringEvent: this.showRecurringFrequency,
      recurrenceType: parseInt(this.eventTypeForm.get('recurringType')?.value),
      timeZone: this.selectedTimezone,
      description: this.eventTypeForm.get('description')?.value,
      isCreateZoomLink: this.createZoomLink,
      eventRecurrenceDTO: {
        type: parseInt(this.eventTypeForm.get('recurringRepeatEveryType')?.value ?? RepeatEveryType.EveryDay),
        frequency: parseInt(this.eventTypeForm.get('recurringRepeatEvery')?.value ?? 0),
        days: this.eventTypeForm.get('recurringRepeatDaysOn')?.value ?? "",
        weekNumber: this.weekCount + 1,
        isWeekOfMonth: this.eventTypeForm.get('recurringRepeatMonthly')?.value == this.reccurrenceMonthlyDateText ? false : true,
        endType: this.eventTypeForm.get('recurringEndsOn')?.value,
        endDateTime: this.eventTypeForm.get('recurringEndsOnDate')?.value,
        endFrequency: parseInt(this.eventTypeForm.get('recurrenceEndsOnOccurrenceCount')?.value ?? 0),
      },
    };

    if(utcEndDateTime) {
      body["EndDateTime"] = utcEndDateTime;
    }

    this.loader.show();
    this.baseService.post(url, body, null, undefined, false).subscribe(data => {
      this.loader.hide();
      this.eventTypeForm.reset();
      this.showClientList = false;
      this.toastrService.success("Event has been successfully created.", "Success");
      if (this.isAdmin) {
        //TODO call to dashboard
        this.router.navigate(["/admin/calendar"]);
      }
      else {
        this.router.navigate(["/therapist/calendar"]);
      }
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  filterTimezone(input) {
  }

  controlValue(controlName: string): string {
    return this.eventTypeForm.get(controlName)?.value;
  }
  
  getEventTypeText(event): string {
    if(event.status.startsWith("1:1 Therapy appointment")) {
      if(this.isAdmin) {


        return EventTypeConstant.TherapySession1on1Admin
          .replace("{{therapist}}", event.therapistName)
          .replace("{{client}}", event.clientName);
      } else {
        return eventTypeText(event.type, event.clientName);
      }
    }

    return event.status;
  }

  goBack() {
    this.location.back();
  }
}
