import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeekViewMobileCalendarComponent } from './week-view-mobile-calendar.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [WeekViewMobileCalendarComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  exports: [
    WeekViewMobileCalendarComponent
  ]
})
export class WeekViewMobileCalendarModule { }
