import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { getStartDateOfWeek } from 'src/app/core/helpers/date-time.helper';

@Component({
  selector: 'week-view-mobile-calendar',
  templateUrl: './week-view-mobile-calendar.component.html',
  styleUrls: ['./week-view-mobile-calendar.component.scss']
})
export class WeekViewMobileCalendarComponent implements OnInit {
  @Input('selectedDate') selectedDate: Date;
  @Input('forwardTraverseInterval') forwardTraverseInterval: number = 7;
  @Input('backwardTraverseInterval') backwardTraverseInterval: number = -7;
  @Output() dateChange = new EventEmitter<WeekValueDateRangeChange>();
  public dateRow: Date[] = [];
  public activeDate: Date;

  constructor() { }

  ngOnInit(): void {
    this.activeDate = this.selectedDate;
    this.onDateSelect(this.activeDate, true);
    this.calculateDatesToBeDisplay();

  }

  calculateDatesToBeDisplay() {
    this.dateRow = [];
    const startDate = getStartDateOfWeek(this.activeDate);
    this.dateRow.push(startDate);
    
    for(let i=1; i<=6; i++) {
      var nextDay = new Date(startDate);
      nextDay.setDate(startDate.getDate() + i);
      this.dateRow.push(nextDay);
    }
  }

  isSameDate(date1, date2): boolean {
    return date1.toDateString() == date2.toDateString();
  }

  traverseDate(interval: number) {
    const nextDate = new Date(this.activeDate);
    nextDate.setDate(this.activeDate.getDate() + interval);
    this.onDateSelect(nextDate);
  }

  onDateSelect(date: Date, forceEmit: boolean = false) {
    if(date.toDateString() != this.activeDate.toDateString() || forceEmit) {
      this.activeDate = date;
      const startDate = getStartDateOfWeek(this.activeDate);
      const endDate = new Date(startDate)
      endDate.setDate(startDate.getDate() + 6);

      this.dateChange.emit(
        {
          startDate,
          endDate,
          activeDate: this.activeDate
        }
      );
      
      this.calculateDatesToBeDisplay();
    }
  }

}

export type WeekValueDateRangeChange = {
  startDate: Date;
  endDate: Date;
  activeDate: Date;
}