import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NavigationExtras, Router } from '@angular/router';
import * as moment from 'moment';
import { MustLoginBaseComponent } from 'src/app/core/helpers/must-login-base-component';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';

@Component({
  selector: 'app-admin-event-info',
  templateUrl: './admin-event-info.component.html',
  styleUrls: ['./admin-event-info.component.scss']
})
export class AdminEventInfoComponent extends MustLoginBaseComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
  ) {
    super(localOpsService, router);
   }

  ngOnInit(): void {
    this.data.isAddSessionNotes = this.data.showAddNotes && this.data.eventType == 1 && this.isTherapist && moment.utc(this.data.dateTime).isBefore(moment().toISOString());
    this.data.isAddActionPlan = this.data.showActionPlan && this.data.eventType == 1 && this.isTherapist && moment.utc(this.data.dateTime).isBefore(moment().toISOString());
    this.data.displayDateTime = moment.utc(this.data.dateTime).format("hh:mm A");
    this.data.displayDate = moment.utc(this.data.dateTime).format("dddd, MMMM DD");

    this.data.showEdit = !moment(this.data.dateTime).isBefore(moment());
  }

  editEvent() {
    
  }

  addSessionNotes() {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'eventId': this.data.eventId }
    };

    if (this.isTherapist)
      this.router.navigate(["therapist/add-notes"], navigationExtras);

    if(this.isAdmin) 
      this.router.navigate(["admin/add-notes"], navigationExtras);
  }
}
