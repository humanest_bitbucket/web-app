import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { GooglePlaceDirective } from "src/app/core/directive/google-place.directive";
import { StateSelectionComponent } from "./state-selection.component";

@NgModule({
    declarations:[
        StateSelectionComponent,
        GooglePlaceDirective
    ],
    imports: [
        ReactiveFormsModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ],
    exports: [StateSelectionComponent]
})
export class StateSelectionModule { }