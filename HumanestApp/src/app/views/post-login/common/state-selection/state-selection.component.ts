import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { GooglePlaceHelper } from 'src/app/core/helpers/google-place.helper';
import { Address } from 'src/app/core/models/address';

@Component({
  selector: 'app-state-selection',
  templateUrl: './state-selection.component.html',
  styleUrls: ['./state-selection.component.scss']
})
export class StateSelectionComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = "fill";
  @Input() form: FormGroup | undefined;
  public address: Address = new Address();;

  constructor() { }

  ngOnInit(): void {
  }

  public getAddress(place: any): void {
    this.address = GooglePlaceHelper.getAddress(place);
    this.form.controls['state'].setValue(this.address.stateName);
  }
}
