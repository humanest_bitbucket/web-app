import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EventApprovalRoutingModule } from './event-approval-routing.module';
import { EventApprovalComponent } from './event-approval.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [
    EventApprovalComponent,
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatTooltipModule,
    AppFormModule,
    EventApprovalRoutingModule,
  ],
  exports: [EventApprovalComponent]
})
export class EventApprovalModule { }
