import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventApprovalComponent } from './event-approval.component';

const routes: Routes = [{
  path: ''
  , component: EventApprovalComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventApprovalRoutingModule { }
