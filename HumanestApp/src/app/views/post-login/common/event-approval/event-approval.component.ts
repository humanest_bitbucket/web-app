import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import * as momenttz from 'moment-timezone';
import { CalendarEventItem } from 'src/app/core/models/calendar-event-item';
import * as moment from 'moment';
import * as _ from "underscore";
import { MustLoginBaseComponent } from 'src/app/core/helpers/must-login-base-component';
import { EventTypeConstant } from 'src/app/core/enums/event-type.enum';
import { eventTypeText } from 'src/app/core/helpers/text-format-helper';
import { MatDialog } from '@angular/material/dialog';
import { AdminEventInfoComponent } from '../admin-event-info/admin-event-info.component';

@Component({
  selector: 'app-event-approval',
  templateUrl: './event-approval.component.html',
  styleUrls: ['./event-approval.component.scss']
})
export class EventApprovalComponent extends MustLoginBaseComponent implements OnInit {
  public eventData: any[];
  public displayedColumns: string[] = ["title", "action"];
  public serverErrors: string[] = [];
  public eventId;

  constructor(
    private readonly loader: LoaderService
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , public readonly dialog: MatDialog
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly route: ActivatedRoute
  ) { 
    super(localOpsService, router);
    this.route.queryParams.subscribe(params => {
      this.eventId = parseInt(params.eventId);
    });
  }

  ngOnInit(): void {
    this.getEvents();
  }

  async getEvents(startDate?: Date, endDate?: Date) {
    this.loader.show();
    try {
      const response = await this.baseService.get("api/Scheduling/GetAdminTherapistPendingEventsByTimezone", 
      { 
        timezone: momenttz.tz.guess(),
        isApproved: false
      }).toPromise();
      this.loader.hide();
      this.eventData = response.data;      

      
      this.eventData = _.map(this.eventData, function(obj, key) {
        const eventTitle = this.getEventTypeText(obj);
        return {
          id: obj.eventId,
          title: eventTitle,
          name: eventTitle,
          start: obj.dateTime,
          ...obj
        } 
        
      }.bind(this));

      if(this.eventId) {
        const filteredEvents = _.filter(this.eventData, (x) => x.eventId == this.eventId);

        if(filteredEvents.length) {
          this.eventData = filteredEvents;
        }
      }
    } 
    catch(errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest && 
        errorResponse.error[0] == "No Events are available.") {
          this.eventData = [];
      } else {
        this.toastrService.error(errorResponse.message);
      }
    }
    finally {
      this.loader.hide();
    }
  }

  getEventTypeText(event): string {
    if(event.name.startsWith("1:1 Therapy appointment")) {
      if(this.isAdmin) {


        return EventTypeConstant.TherapySession1on1Admin
          .replace("{{therapist}}", event.therapistName)
          .replace("{{client}}", event.clientName);
      } else {
        return eventTypeText(event.eventType, event.clientName);
      }
    }

    return event.name;
  }

  
  updateEvent(isApproved: boolean, eventData) {
    let url = "api/Scheduling/saveevent";
    let body = {
      isApproved,
      eventId: eventData.eventId,
      timezone: momenttz.tz.guess(),
    };
  
    this.loader.show();
    this.baseService.post(url, body, null, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("Event has been successfully " + (isApproved ? 'Approved' : 'Denied'), 
        "Success");
      this.getEvents();
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  showEventInformation(eventdata) {
    this.dialog.open(AdminEventInfoComponent, {
      data: {
        eventName: eventdata.title,
        ...eventdata
      },
    });
  }
}
