import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/core/services/auth-guard.service';
import { UpdatePasswordComponent } from './update-password.component';

const routes: Routes = [{
  path: ''
  , component: UpdatePasswordComponent
  , canActivate: [AuthGuardService]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class UpdatePasswordRoutingModule { }
