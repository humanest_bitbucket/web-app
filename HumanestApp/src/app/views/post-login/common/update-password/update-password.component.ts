import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { MustLoginBaseComponent } from 'src/app/core/helpers/must-login-base-component';
import { APIResponse } from 'src/app/core/models/api-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';

@Component({
  selector: 'update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent extends MustLoginBaseComponent {
  public title = 'Update-Password';
  public updatePasswordForm: FormGroup;
  public serverErrors: string[];

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly loader: LoaderService) {
      super(localOpsService, router);
    this.updatePasswordForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    this.validateLogin();
    this.setUpUpdatePasswordForm();
  }

  setUpUpdatePasswordForm(): void {
    this.updatePasswordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(8)]]
      , confirmPassword: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  onSubmit(){
    if(ValidationHelper.showFieldsError(this.updatePasswordForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/account/updateinitialpassword";
    let body = {
      "password": this.updatePasswordForm.get('password')?.value,
      "confirmPassword": this.updatePasswordForm.get('confirmPassword')?.value
    };
    
    this.loader.show();
    this.baseService.post(url,body, null, undefined, false).subscribe((data: APIResponse) => {
      this.loader.hide();
      this.toastrService.success("Password has been successfully updated.", "Success");
      this.updatePasswordForm.reset();
      this.localOpsService.saveAccessToken(data.accessToken);
      this.localOpsService.fetchAccessTokenDetailsAsUserDetails();

      let accessTokenModel = this.localOpsService.fetchClaimDetailsFromAccessToken();

      if(accessTokenModel.role === UserRoleConstant.admin) {
          //TODO: redirect to dashboard
          this.router.navigate(["/admin/calendar"]);
      }
      else if(accessTokenModel.role === UserRoleConstant.therapist || accessTokenModel.role === UserRoleConstant.counselor) {
        if(!accessTokenModel.isProfileUpdated) {
          this.router.navigate(["/therapist/updateprofile"]);
        }
        else {
          //TODO: redirect to dashboard
          this.router.navigate(["/therapist/calendar"]);
        }
      }
    }, (errorResponse) => {
      this.loader.hide();
      if(errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }
}
