import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpdatePasswordRoutingModule } from './update-password-routing.module';
import { UpdatePasswordComponent } from './update-password.component';
import { ToastrModule } from 'ngx-toastr';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [UpdatePasswordComponent],
  imports: [
    CommonModule,
    UpdatePasswordRoutingModule,
    AppFormModule,
    ToastrModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule
  ]
})
export class UpdatePasswordModule { }
