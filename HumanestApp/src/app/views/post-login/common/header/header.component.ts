import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MustLoginBaseComponent } from 'src/app/core/helpers/must-login-base-component';
import { AccessToken } from 'src/app/core/models/access-token';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';

@Component({
  selector: 'nav-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends MustLoginBaseComponent {
  public title = 'Header';
  private userData = new AccessToken();
  
  constructor(private readonly localOpsService: LocalOpsService
    , private readonly router: Router) {
      super(localOpsService, router);
     }

  ngOnInit() {
    this.validateLogin();
    this.userData = this.localOpsService.fetchClaimDetailsFromAccessToken();
  }

  logout(): void{
    this.localOpsService.clearLocalStorage();
    this.router.navigate(["/login"]);
  }

  getPath(subPath: string): string {
    return this.userData.role + "/" + subPath;
  }

  get userRole() {
    return this.userData.role;
  }
}
