import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { MustLoginBaseComponent } from 'src/app/core/helpers/must-login-base-component';
import { ClientDataResponse } from 'src/app/core/models/client-data-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';

@Component({
  selector: 'app-view-session-notes',
  templateUrl: './view-session-notes.component.html',
  styleUrls: ['./view-session-notes.component.scss']
})
export class ViewSessionNotesComponent extends MustLoginBaseComponent implements OnInit {

  public sessionNoteForm: FormGroup;
  public serverErrors: string[];
  public sessionNoteList: any[];
  public clientList: Array<{ id: string, value: string }> = [];
  public filteredOptionsClient: Observable<any[]>;
  public selectedClient = "";

  constructor(
    private readonly loader: LoaderService
    , private readonly formBuilder: FormBuilder
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly localOpsService: LocalOpsService
    , private readonly router: Router) { 
      super(localOpsService, router);
    }

  async ngOnInit() {
    this.sessionNoteForm = this.formBuilder.group({
      client: ['', [Validators.required]],
    });
    await this.getSessionNotes();
    await this.getClientsList();
  }

  async getSessionNotes() {
    
    this.loader.show();
    try {
      const url = "api/Scheduling/GetTherapistSessionNotes";

      const response = await this.baseService.get(url, { clientId: this.selectedClient }).toPromise();
      console.log(response);

      this.sessionNoteList = response.data;
    } 
    catch(errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
          
        this.serverErrors = errorResponse.error;
      } else {
        this.toastrService.error(errorResponse.message);
      }
    }
    finally {
      this.loader.hide();
    }
  }

  getClientsList() {
    this.serverErrors = [];
    this.clientList = [];
    let url = "api/client/Client/getclients";

    this.loader.show();

    this.baseService.get(url, null, undefined, false).subscribe((data: ClientDataResponse) => {
      this.loader.hide();
      data.data.forEach(item => {
        let value = item.firstName + ' ' + item.lastName + ' (' + item.email + ')';
        this.clientList.push({ id: item.id, value: value });
      });
      
      this.filteredOptionsClient = this.sessionNoteForm.get('client')!.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(this.clientList, "value", value)),
      );

    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  setClient(value: string) {
    this.selectedClient = value;
    this.getSessionNotes();
  }

  private _filter(filterList, prop, value: any) {
    const filterValue = value[prop] ? value[prop].toLowerCase() : value?.toLowerCase();

    const matchedOptions = filterList.filter(option => option[prop].toLowerCase().includes(filterValue));

    return [...matchedOptions];
  }
}
