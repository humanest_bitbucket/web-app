import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewSessionNotesComponent } from './view-session-notes.component';


const routes: Routes = [{
  path: ''
  , component: ViewSessionNotesComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewSessionNotesRoutingModule { }
