import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatDialogModule } from "@angular/material/dialog";
import { MatDividerModule } from "@angular/material/divider";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { AppFormModule } from "src/app/views/shared/app-form/app-form.module";
import { ViewSessionNotesComponent } from "./view-session-notes.component";
import { ViewSessionNotesRoutingModule } from "./view-session-routing.module";


@NgModule({
  declarations: [ViewSessionNotesComponent],
  imports: [
    CommonModule,
    ViewSessionNotesRoutingModule,
    AppFormModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatAutocompleteModule,
    MatDividerModule
  ],
  exports: [
    ViewSessionNotesComponent
  ]
})
export class ViewSessionNotesModule { }
