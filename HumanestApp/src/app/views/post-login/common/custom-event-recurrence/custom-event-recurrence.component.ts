import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CalendarConstant } from 'src/app/core/constants/calendar.constant';
import { DaysList, DaysListType } from 'src/app/core/enums/day-list.enum';
import { RecurringTypeMapping, RepeatEveryType, RepeatEveryTypeMapping } from 'src/app/core/enums/recurring-type.enum';

@Component({
  selector: 'app-custom-event-recurrence',
  templateUrl: './custom-event-recurrence.component.html',
  styleUrls: ['./custom-event-recurrence.component.scss']
})
export class CustomEventRecurrenceComponent implements OnInit {

  public recurrenceForm: FormGroup;
  public eventTypeForm: FormGroup;
  public dayList = DaysList.daysList;
  public recurringRepeatEveryTypes = RepeatEveryTypeMapping.repeatEveryTypes;
  public recurrenceEndsOnDate: boolean = false;
  public recurrenceEndsOnOccurrence: boolean = false;
  public recurringRepeatEveryWeek: boolean = false;
  public recurringRepeatEveryMonth: boolean = false;
  public minDate: Date = new Date();
  public endRadioValue: string = "Never";
  public reccurrenceMonthlyDateText = CalendarConstant.monthlyDate;
  public reccurrenceMonthlyWeekdayText = CalendarConstant.monthlyWeekDay;

  constructor(
    private readonly formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.recurrenceForm = this.data.recurringFormGroup;
      this.eventTypeForm = this.data.eventTypeForm;
   }

  ngOnInit(): void {
  }

  
  updateRecurringRepeatEveryText(e: any) {
    let selectedValue = e.target.value;
    if (selectedValue == 1) {
      this.recurringRepeatEveryTypes.forEach(item => { if (item.text.endsWith('s')) { item.text = item.text.substring(0, item.text.length - 1) } });
    }
    else {
      this.recurringRepeatEveryTypes.forEach(item => { if (!item.text.endsWith('s')) { item.text = item.text + 's' } });
    }
  }

  changeRecurringEndsOn(event: any): void {
    let selectedValue = event.value;
    this.recurrenceEndsOnDate = false;

    if(selectedValue)
      this.recurrenceForm.get("recurrenceEndsOnOccurrenceCount")?.disable();
    
      this.recurrenceEndsOnOccurrence = false;
    if (selectedValue == 'On') {
      this.recurrenceEndsOnDate = true;
    }
    else if (selectedValue == 'After') {
      this.recurrenceEndsOnOccurrence = true;
      this.recurrenceForm.get("recurrenceEndsOnOccurrenceCount")?.enable();
    }
  }

  changeRecurringRepeatEveryType(e: any): void {
    let selectedIndex = e.value;
    this.recurringRepeatEveryWeek = false;
    this.recurringRepeatEveryMonth = false;
    if (selectedIndex == RepeatEveryType.EveryWeek) {
      this.recurringRepeatEveryWeek = true;
    }
    else if (selectedIndex == RepeatEveryType.EveryMonth) {
      this.recurringRepeatEveryMonth = true;
    }
  }

  setActive(day: DaysListType) {
    day.active = !day.active;
  }

  saveRecurrencePopUp() {
    let arr = this.dayList.filter(data => data.active == true);
    let selectedDays: string = "";

    arr.forEach(element => {
      selectedDays = selectedDays + element.shortText + ',';
    });

    this.eventTypeForm.get('recurringRepeatDaysOn')?.setValue(selectedDays.replace(/,\s*$/, ""));
    this.dialogRef.close();
  }

  closeRecurrencePopUp() {
    this.eventTypeForm.get('recurringType')?.setValue(RecurringTypeMapping.recurringTypes[0].id);
    this.dialogRef.close();
  }

}
