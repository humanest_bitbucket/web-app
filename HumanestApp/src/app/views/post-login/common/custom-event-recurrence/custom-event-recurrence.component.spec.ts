import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomEventRecurrenceComponent } from './custom-event-recurrence.component';

describe('CustomEventRecurrenceComponent', () => {
  let component: CustomEventRecurrenceComponent;
  let fixture: ComponentFixture<CustomEventRecurrenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomEventRecurrenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomEventRecurrenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
