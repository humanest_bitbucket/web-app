import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CLASSIC_EDITOR_WITHOUT_MEDIA } from 'src/app/core/constants/ckeditor.constant';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { ClientDataResponse } from 'src/app/core/models/client-data-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-session-notes',
  templateUrl: './add-session-notes.component.html',
  styleUrls: ['./add-session-notes.component.scss']
})
export class AddSessionNotesComponent implements OnInit {

  public sessionNoteForm: FormGroup;
  public Editor = ClassicEditor;
  public config;
  public serverErrors: string[];
  // public clientList: Array<{ id: string, value: string }> = [];
  // public filteredOptionsClient: Observable<any[]>;
  // public selectedClient;
  private eventId: string;

  constructor(
    private readonly formBuilder: FormBuilder
    , private readonly loader: LoaderService
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly location: Location
    , private route: ActivatedRoute) {
    ClassicEditor.defaultConfig = CLASSIC_EDITOR_WITHOUT_MEDIA;
    this.route.queryParams.subscribe(params => {
      this.eventId = params['eventId'];
    });
    this.setEventTypeForm();
   }

  async ngOnInit() {
    // await this.getClientsList();
  }

  setEventTypeForm(): void {
    this.sessionNoteForm = this.formBuilder.group({
      // client: ['', [Validators.required]],
      sessioNote: ['', [Validators.required]],
    });
  }

  
  // getClientsList() {
  //   this.serverErrors = [];
  //   this.clientList = [];
  //   let url = "api/client/Client/getclients";

  //   this.loader.show();

  //   this.baseService.get(url, null, undefined, false).subscribe((data: ClientDataResponse) => {
  //     this.loader.hide();
  //     data.data.forEach(item => {
  //       let value = item.firstName + ' ' + item.lastName + ' (' + item.email + ')';
  //       this.clientList.push({ id: item.id, value: value });
  //     });
      
  //     this.filteredOptionsClient = this.sessionNoteForm.get('client')!.valueChanges.pipe(
  //       startWith(''),
  //       map(value => this._filter(this.clientList, "value", value)),
  //     );

  //   }, (errorResponse) => {
  //     this.loader.hide();
  //     if (errorResponse.status === HttpStatusConstant.badRequest) {
  //       this.serverErrors = errorResponse.error;
  //     }
  //   });
  // }

  // setClient(value: string) {
  //   this.selectedClient = value;
  // }

  async onSubmit() {

    if(!this.sessionNoteForm.get('sessioNote').value) {
      this.sessionNoteForm.markAllAsTouched();
      return;
    }

    this.loader.show();
    try {
      const url = "api/Scheduling/addtherapistsessionnotes";
      const requestBody = { 
        eventId: this.eventId,
        sessionNotes: this.sessionNoteForm.get('sessioNote').value
      };

      await this.baseService.post(url, null, requestBody).toPromise();
      this.toastrService.success("Session notes has been saved successfully.", "Success");
      
      this.location.back();
    } 
    catch(errorResponse) {
      if (errorResponse.status === HttpStatusConstant.badRequest) {
          
        this.serverErrors = errorResponse.error;
      } else {
        this.toastrService.error(errorResponse.message);
      }
    }
    finally {
      this.loader.hide();
    }
  }
  
  // private _filter(filterList, prop, value: any) {
  //   const filterValue = value[prop] ? value[prop].toLowerCase() : value?.toLowerCase();

  //   const matchedOptions = filterList.filter(option => option[prop].toLowerCase().includes(filterValue));

  //   return [...matchedOptions];
  // }

}
