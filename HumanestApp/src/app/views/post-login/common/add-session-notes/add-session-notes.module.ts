import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { AppFormModule } from "src/app/views/shared/app-form/app-form.module";
import { AddSessionNotesComponent } from "./add-session-notes.component";
import { AddSessionNotesRoutingModule } from "./add-session-routing.module";


@NgModule({
  declarations: [AddSessionNotesComponent],
  imports: [
    CommonModule,
    AddSessionNotesRoutingModule,
    AppFormModule,
    CKEditorModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatAutocompleteModule,
  ],
  exports: [
    AddSessionNotesComponent
  ]
})
export class AddSessionNotesModule { }
