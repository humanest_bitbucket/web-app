import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddSessionNotesComponent } from './add-session-notes.component';


const routes: Routes = [{
  path: ''
  , component: AddSessionNotesComponent
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddSessionNotesRoutingModule { }
