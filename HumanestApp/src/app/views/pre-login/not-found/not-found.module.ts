import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgOtpInputModule } from 'ng-otp-input';
import { NotFoundComponent } from './not-found.component';
import { NotFoundRoutingModule } from './not-found-routing.module';


@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    CommonModule,
    NotFoundRoutingModule,
    AppFormModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    NgOtpInputModule
  ]
})
export class NotFoundModule { }
