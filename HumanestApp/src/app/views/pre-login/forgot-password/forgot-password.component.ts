import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from '../../shared/app-form/helpers/validation.helper';

@Component({
  selector: 'forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
  public title = 'Forgot-Password';
  public showEmailDiv: boolean = true;
  public serverErrors: string[];
  public resetPasswordForm: FormGroup;
  public sendResetLinkForm: FormGroup;

  constructor(private route: ActivatedRoute
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly formBuilder: FormBuilder
    , private readonly loader: LoaderService) {
    this.serverErrors = [];
    this.sendResetLinkForm = new FormGroup({});
    this.resetPasswordForm = new FormGroup({});
  }

  ngOnInit(): void {
    this.setUpForms();
    if (this.route.snapshot.queryParams['token'] != null)
      this.validateToken();
  }

  setUpForms(): void {
    this.sendResetLinkForm = this.formBuilder.group({
      emailAddress: ['', [Validators.required, ValidationHelper.emailValidator]]
    });

    this.resetPasswordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(8)]]
      , confirmPassword: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  public sendResetLink = () => {
    if (ValidationHelper.showFieldsError(this.sendResetLinkForm)) {
      return;
    }
    this.serverErrors = [];
    let url = "api/account/forgotpassword";

    this.loader.show();
    this.baseService.get(url, { email: this.sendResetLinkForm.get('emailAddress')?.value }, undefined, true).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("Reset password link is sent successfully.", "Success");
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  private validateToken = () => {
    this.serverErrors = [];

    const token = this.route.snapshot.queryParams['token'];
    const email = this.route.snapshot.queryParams['email'];

    let url = "api/account/validatepasswordresettoken";

    this.loader.show();
    this.baseService.get(url, { email: email, token: token }, undefined, true).subscribe(data => {
      this.loader.hide();
      this.showEmailDiv = false;
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
        this.showEmailDiv = true;
      }
    });
  }

  public onSubmit = () => {
    if (ValidationHelper.showFieldsError(this.resetPasswordForm)) {
      return;
    }
    this.serverErrors = [];

    const token = this.route.snapshot.queryParams['token'];
    const email = this.route.snapshot.queryParams['email'];

    let url = "api/account/resetpassword";
    let body = {
      email: email,
      newpassword: this.resetPasswordForm.get('password')?.value,
      confirmpassword: this.resetPasswordForm.get('confirmPassword')?.value,
      resetpasswordtoken: token
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, true).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("Password changed successfully.", "Success");
      this.router.navigate(["/login"]);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }
}
