import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';
import { AppFormModule } from '../../shared/app-form/app-form.module';
import { ToastrModule } from 'ngx-toastr';
import { HeaderModule } from '../header/header.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';


@NgModule({
  declarations: [ForgotPasswordComponent],
  imports: [
    CommonModule,
    ForgotPasswordRoutingModule,
    AppFormModule,
    ToastrModule,
    HeaderModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
  ]
})
export class ForgotPasswordModule { }
