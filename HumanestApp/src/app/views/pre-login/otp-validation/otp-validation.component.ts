import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { timer } from 'rxjs';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { OTPPurposeConstant } from 'src/app/core/constants/otp-purpose.constants';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { MustLoginBaseComponent } from 'src/app/core/helpers/must-login-base-component';
import { APIResponse } from 'src/app/core/models/api-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';

@Component({
  selector: 'otp-validation',
  templateUrl: './otp-validation.component.html',
  styleUrls: ['./otp-validation.component.scss']
})
export class OtpValidationComponent extends MustLoginBaseComponent {
  public title = 'OTP-Validation';
  public otpValidationForm: FormGroup;
  private timeLeft: number = 5;
  public subscribeTimer: number = 5;
  public enableResendOTP: boolean = false;
  public serverErrors: string[];
  @ViewChild('ngOtpInput') ngOtpInputRef:any;

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly loader: LoaderService) {
      super(localOpsService, router);
    this.otpValidationForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    this.validateLogin();
    this.setTimer();
    this.setOtpValidationForm();
  }

  setOtpValidationForm(): void {
    this.otpValidationForm = this.formBuilder.group({
      otp: ['', [Validators.minLength(4), Validators.maxLength(4)]]
    });
  }

  resendOTP(){
    this.setTimer();  
    this.ngOtpInputRef.setValue("");  
    this.enableResendOTP = false;
    this.serverErrors = [];
    this.otpValidationForm.reset();

    let url = "api/account/generateotp";
    
    this.loader.show();
    this.baseService.get(url, {Purpose:OTPPurposeConstant.login}, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("OTP has been sent successfully.", "Success");
    }, (errorResponse) => {
      this.loader.hide();
      if(errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  onSubmit(){
    if(ValidationHelper.showFieldsError(this.otpValidationForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/account/validateotp";
    let params = {
      otp: this.otpValidationForm.get('otp')?.value,
      purpose: OTPPurposeConstant.login
    };
    
    this.loader.show();
    this.baseService.post(url,null, params, undefined, false).subscribe((data: APIResponse)  => {
      this.loader.hide();
      this.toastrService.success("OTP has been successfully validated.", "Success");

      this.localOpsService.saveAccessToken(data.accessToken);
      this.localOpsService.fetchAccessTokenDetailsAsUserDetails();

      let accessTokenModel = this.localOpsService.fetchClaimDetailsFromAccessToken();

      if(accessTokenModel.role === UserRoleConstant.admin) {

        if(!accessTokenModel.isPasswordUpdated) {
          this.router.navigate(["/updatepassword"]);
        }
        else {
          //TODO: redirect to dashboard
          this.router.navigate(["/admin/calendar"]);
        }
      }
      else if(accessTokenModel.role === UserRoleConstant.therapist || accessTokenModel.role === UserRoleConstant.counselor) {
        if(!accessTokenModel.isPasswordUpdated) {
          this.router.navigate(["/updatepassword"]);
        }
        else if(!accessTokenModel.isProfileUpdated) {
          this.router.navigate(["/therapist/updateprofile"]);
        }
        else {
          //TODO: redirect to dashboard
          this.router.navigate(["/therapist/calendar"]);
        }
      }
    }, (errorResponse) => {
      this.loader.hide();
      if(errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  setTimer() {
    this.timeLeft = 5;
    this.subscribeTimer = 5;
    const source = timer(1000, 1000);
    const ticks = source.subscribe(val => {
      if(this.subscribeTimer <= 0){
        this.subscribeTimer = 0;
        this.enableResendOTP = true;
        ticks.unsubscribe();
      }
      else{
      this.subscribeTimer = this.timeLeft - val;
    }
    });
  }

  onOtpChange(value: string, inputElem: HTMLInputElement): void {
    this.otpValidationForm.controls.otp.setValue(value);
  }
}
