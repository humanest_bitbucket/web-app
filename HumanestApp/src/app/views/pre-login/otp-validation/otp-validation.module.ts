import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OtpValidationRoutingModule } from './otp-validation-routing.module';
import { OtpValidationComponent } from './otp-validation.component';
import { ToastrModule } from 'ngx-toastr';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { NgOtpInputModule } from 'ng-otp-input';


@NgModule({
  declarations: [OtpValidationComponent],
  imports: [
    CommonModule,
    OtpValidationRoutingModule,
    AppFormModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    NgOtpInputModule
  ]
})
export class OtpValidationModule { }
