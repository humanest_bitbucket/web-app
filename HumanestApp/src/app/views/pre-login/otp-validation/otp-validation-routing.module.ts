import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OtpValidationComponent } from './otp-validation.component';

const routes: Routes = [{
  path: ''
  , component: OtpValidationComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtpValidationRoutingModule { }
