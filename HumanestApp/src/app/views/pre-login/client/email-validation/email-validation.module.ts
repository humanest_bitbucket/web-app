import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmailValidationRoutingModule } from './email-validation-routing.module';
import { EmailValidationComponent } from './email-validation.component';
import { AppFormModule } from 'src/app/views/shared/app-form/app-form.module';


@NgModule({
  declarations: [
    EmailValidationComponent
  ],
  imports: [
    CommonModule,
    EmailValidationRoutingModule,
    AppFormModule
  ]
})
export class EmailValidationModule { }
