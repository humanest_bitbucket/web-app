import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { APIResponse } from 'src/app/core/models/api-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';

@Component({
  selector: 'email-validation',
  templateUrl: './email-validation.component.html',
  styleUrls: ['./email-validation.component.scss']
})
export class EmailValidationComponent {
  public title = 'Email-Validation';
  public showSuccess: boolean = false;
  public showError: boolean = false;
  public showEmailAlreadySent: boolean = true;
  public serverErrors: string[];

  constructor(private route: ActivatedRoute
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly loader: LoaderService) {
    this.serverErrors = [];
  }

  ngOnInit(): void {
    if (this.route.snapshot.queryParams['token'] != null)
      this.confirmEmail();
  }

  private confirmEmail = () => {
    this.showError = this.showSuccess = this.showEmailAlreadySent = false;
    const token = this.route.snapshot.queryParams['token'];
    const email = this.route.snapshot.queryParams['email'];
    this.serverErrors = [];

    let url = "api/account/confirmemail";

    this.loader.show();
    this.baseService.post(url, null, { email: email, emailConfirmationToken: token }, undefined, true).subscribe((data: APIResponse) => {
      this.loader.hide();
      this.showSuccess = true;
      this.toastrService.success("Email verification is successful.", "Success");

      this.localOpsService.saveAccessToken(data.accessToken);
      this.localOpsService.fetchAccessTokenDetailsAsUserDetails();

      this.router.navigate(["/client/initialprofile"]);
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
        this.showError = true;
      }
    });
  }

  public resendConfirmationEmail = () => {
    this.showError = this.showSuccess = false;
    this.serverErrors = [];
    let url = "api/account/resendConfirmationEmail";

    this.loader.show();
    this.baseService.post(url, null, null, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("Verification link is sent successfully.", "Success");
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
        this.showError = true;
      }
    });
  }
}
