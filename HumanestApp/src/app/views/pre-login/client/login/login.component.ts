import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { OTPPurposeConstant } from 'src/app/core/constants/otp-purpose.constants';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { APIResponse } from 'src/app/core/models/api-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from '../../../shared/app-form/helpers/validation.helper';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  public title = 'Login';
  public loginForm: FormGroup;
  public serverErrors: string[];

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly loader: LoaderService) {
    this.loginForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    const userData = this.localOpsService.fetchClaimDetailsFromAccessToken();
    if(userData?.userId) {
      this.logout();
    }
    this.setUpLoginForm();
  }

  setUpLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      emailAddress: ['', [Validators.required, ValidationHelper.emailValidator]]
      , password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  getMagicLink() {
    this.serverErrors = [];
    this.loginForm.controls['emailAddress'].markAsTouched({ onlySelf: false });
    if (!this.loginForm.controls['emailAddress'].valid) {
      return
    }

    let url = "api/account/login";
    let body = {
      email: this.loginForm.get('emailAddress')?.value,
      isMagicLink: true
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, true).subscribe((data: APIResponse) => {
      this.toastrService.success("Magic link is sent successfully.", "Success");
      this.loader.hide();
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  onSubmit() {
    if (ValidationHelper.showFieldsError(this.loginForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/account/login";
    let body = {
      email: this.loginForm.get('emailAddress')?.value,
      password: this.loginForm.get('password')?.value
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, true).subscribe((data: APIResponse) => {
      this.loader.hide();
      this.toastrService.success("Login successful.", "Success");

      this.localOpsService.saveAccessToken(data.accessToken);
      this.localOpsService.fetchAccessTokenDetailsAsUserDetails();

      let accessTokenModel = this.localOpsService.fetchClaimDetailsFromAccessToken();

      if (accessTokenModel.role === UserRoleConstant.admin || accessTokenModel.role === UserRoleConstant.therapist || accessTokenModel.role === UserRoleConstant.counselor) {
        this.requestOTP();
        this.router.navigate(["/otp"]);
      }
      else if (accessTokenModel.role === UserRoleConstant.client) {
        if (!accessTokenModel.isEmailConfirmed) {
          this.router.navigate(["/emailvalidation"]);
        }
        else if (!accessTokenModel.isProfileUpdated) {
          this.router.navigate(["/client/initialprofile"]);
        }
        else {
          this.router.navigate(["/client"]);
        }
      }
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }

  requestOTP() {
    this.serverErrors = [];
    let url = "api/account/generateotp";

    this.loader.show();
    this.baseService.get(url, { purpose: OTPPurposeConstant.login }, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("OTP has been successfully sent.", "Success");
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }
  
  logout(): void{
    this.localOpsService.clearLocalStorage();
    this.router.navigate(["/login"]);
  }
}
