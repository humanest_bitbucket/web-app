import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { BaseService } from 'src/app/core/services/base.service';
import { LoaderService } from 'src/app/loader.service';
import { ValidationHelper } from 'src/app/views/shared/app-form/helpers/validation.helper';
@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {
  public title = 'Registration';
  public clientRegistrationForm: FormGroup;
  public serverErrors: string[];

  constructor(private readonly baseService: BaseService
    , private readonly formBuilder: FormBuilder
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly loader: LoaderService) {
    this.clientRegistrationForm = new FormGroup({});
    this.serverErrors = [];
  }

  ngOnInit(): void {
    this.setUpClientRegistrationForm();
  }

  setUpClientRegistrationForm(): void {
    this.clientRegistrationForm = this.formBuilder.group({
      emailAddress: ['', [Validators.required, ValidationHelper.emailValidator]]
      , password: ['', [Validators.required, Validators.minLength(8)]],
      code: ['']
    });
  }

  onSubmit() {
    if (ValidationHelper.showFieldsError(this.clientRegistrationForm)) {
      return;
    }
    this.serverErrors = [];

    let url = "api/account/registerclient";
    let body = {
      "email": this.clientRegistrationForm.get('emailAddress')?.value,
      "password": this.clientRegistrationForm.get('password')?.value,
      "schoolCode": this.clientRegistrationForm.get('code')?.value,
      "role": UserRoleConstant.client
    };

    this.loader.show();
    this.baseService.post(url, body, null, undefined, true).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("Registration successful.", "Success");
      this.clientRegistrationForm.reset();
      
      this.router.navigate(["/login"]);
    }, (errorResponse) => {
      this.loader.hide();
      if(errorResponse.status === HttpStatusConstant.badRequest) {
        this.serverErrors = errorResponse.error;
      }
    });
  }
}
