import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from 'src/app/core/services/auth-guard.service';
import { ClientComponent } from './client.component';

const routes: Routes = [
  {
    path: ''
    , component: ClientComponent
    , children: [
      {
        path: ''
        , redirectTo: 'home'
        , pathMatch: 'full'
      }
      , {
        path: 'register'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./registration/registration.module').then(m => m.RegistrationModule)
      }
      , {
        path: 'home'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      }, {
        path: 'login'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
      }
      , {
        path: 'resetpassword'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('../forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule)
      }  
      , {
        path: 'otp'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('../otp-validation/otp-validation.module').then(m => m.OtpValidationModule)
      }
      , {
        path: 'emailvalidation'
        , canActivate: [AuthGuardService]
        , loadChildren: () => import('./email-validation/email-validation.module').then(m => m.EmailValidationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class ClientRoutingModule { }
