import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MagicLinkRoutingModule } from './magic-link-routing.module';
import { MagicLinkComponent } from './magic-link.component';
import { HeaderModule } from '../header/header.module';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [MagicLinkComponent],
  imports: [
    CommonModule,
    MagicLinkRoutingModule,
    MatCardModule,
    HeaderModule
  ]
})
export class MagicLinkModule { }
