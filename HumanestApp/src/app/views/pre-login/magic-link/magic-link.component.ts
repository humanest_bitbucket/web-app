import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpStatusConstant } from 'src/app/core/constants/http-status.constant';
import { OTPPurposeConstant } from 'src/app/core/constants/otp-purpose.constants';
import { UserRoleConstant } from 'src/app/core/constants/user-role.constant';
import { MustLoginBaseComponent } from 'src/app/core/helpers/must-login-base-component';
import { APIResponse } from 'src/app/core/models/api-response';
import { BaseService } from 'src/app/core/services/base.service';
import { LocalOpsService } from 'src/app/core/services/local-ops.service';
import { LoaderService } from 'src/app/loader.service';

@Component({
  selector: 'magic-link',
  templateUrl: './magic-link.component.html',
  styleUrls: ['./magic-link.component.scss']
})
export class MagicLinkComponent extends MustLoginBaseComponent {
  public title = 'Magic-Link';
  public showError: boolean = false;

  constructor(private route: ActivatedRoute
    , private readonly baseService: BaseService
    , private readonly toastrService: ToastrService
    , private readonly router: Router
    , private readonly localOpsService: LocalOpsService
    , private readonly loader: LoaderService) {
      super(localOpsService, router);
  }

  ngOnInit(): void {
    this.validateLogin();
    this.verifyMagicLink();
  }

  private verifyMagicLink = () => {
    this.showError = false;

    let url = "api/account/validatemagiclink";
    const token = this.route.snapshot.queryParams['token'];
    const email = this.route.snapshot.queryParams['email'];

    this.loader.show();
    this.baseService.post(url, null, { magicLinkToken: token, email: email }, undefined, true).subscribe((data: APIResponse) => {
      this.loader.hide();
      this.toastrService.success("Login successful.", "Success");

      this.localOpsService.saveAccessToken(data.accessToken);
      this.localOpsService.fetchAccessTokenDetailsAsUserDetails();

      let accessTokenModel = this.localOpsService.fetchClaimDetailsFromAccessToken();

      if (accessTokenModel.role === UserRoleConstant.admin || accessTokenModel.role === UserRoleConstant.therapist || accessTokenModel.role === UserRoleConstant.counselor) {
        this.requestOTP();
        this.router.navigate(["/otp"]);
      }
      else if (accessTokenModel.role === UserRoleConstant.client) {
        if (!accessTokenModel.isEmailConfirmed) {
          //TODO call to email verification
          this.router.navigate(["/emailvalidation"]);
        }
        else if (!accessTokenModel.isProfileUpdated) {
          this.router.navigate(["/client/initialprofile"]);
        }
        else {
          //TODO: redirect to dashboard
          this.router.navigate(["/client/dashboard"]);
        }
      }
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.showError = true;
      }
    });
  }

  requestOTP() {
    this.showError = false;
    let url = "api/account/generateotp";

    this.loader.show();
    this.baseService.get(url, { Purpose: OTPPurposeConstant.login }, undefined, false).subscribe(data => {
      this.loader.hide();
      this.toastrService.success("OTP has been successfully sent.", "Success");
    }, (errorResponse) => {
      this.loader.hide();
      if (errorResponse.status === HttpStatusConstant.badRequest) {
        this.showError = true;
      }
    });
  }
}
