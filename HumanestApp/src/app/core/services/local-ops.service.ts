import { Location } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { StorageConstant } from "../constants/storage.constant";
import { AccessToken } from "../models/access-token";
import { BaseService } from "./base.service";
import { StorageService } from "./storage-services/storage.service";
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from "rxjs";
import { CounselorFilter } from "../models/counselor-filters";

@Injectable({
    providedIn: 'root'
})
export class LocalOpsService extends BaseService {
    constructor(public httpClient: HttpClient
        , public storageService: StorageService
        , public location: Location
        , public router: Router
        , public toastrService: ToastrService) {
        super(httpClient, storageService, location, router, toastrService);
    }

    public accessModel: BehaviorSubject<AccessToken> = new BehaviorSubject<AccessToken>({});
    public counselorFilterModel: BehaviorSubject<CounselorFilter> = new BehaviorSubject<CounselorFilter>({});

    public clearLocalStorage(): void {
        this.storageService.removeItem(StorageConstant.accessToken);
        this.storageService.removeItem(StorageConstant.counselorFilter);
        this.storageService.clear();
    }

    public saveAccessToken(token: string): void {
        if (token) {
            this.clearLocalStorage();
            this.storageService.setItem(StorageConstant.accessToken, token);
            BaseService.accessTokenSubject.next(token);
        }
    }

    public fetchClaimDetailsFromAccessToken(): AccessToken {
        let accessToken = new AccessToken();
        let claims = super.getClaimsFromToken();
        if (claims) {
            if (!super.isTokenExpired()) {
                accessToken.username = claims['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'];
                accessToken.role = claims['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
                accessToken.email = claims['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'];
                accessToken.userId = claims['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier'];
                accessToken.refreshToken = claims['refreshToken'];
                accessToken.fullName = claims['fullName'];
                accessToken.firstName = claims['firstName'];
                accessToken.lastName = claims['lastName'];
                accessToken.isProfileUpdated = this.getBoolValue(claims['isProfileUpdated']);
                accessToken.isOTPValidated = this.getBoolValue(claims['isOTPValidated']);
                accessToken.isPasswordUpdated = this.getBoolValue(claims['isPasswordUpdated']);
                accessToken.isEmailConfirmed = this.getBoolValue(claims['isEmailConfirmed']);
            }
        }
        return accessToken;
    }

    public fetchAccessTokenDetailsAsUserDetails(): void {
        let accessTokenDetails = this.fetchClaimDetailsFromAccessToken();
        this.accessModel.next(accessTokenDetails);
    }

    public fetchAccessToken(): string {
        return this.storageService.getItem(StorageConstant.accessToken);
    }

    private getBoolValue(value: string): boolean{
        return (value?.toLowerCase() == "true");
    }

    public clearCounselorFilter(): void {
        this.storageService.removeItem(StorageConstant.counselorFilter);
    }

    public saveCounselorFilter(counselorFilter: CounselorFilter): void {
        if (counselorFilter) {
            this.clearCounselorFilter();
            this.storageService.setItem(StorageConstant.counselorFilter, JSON.stringify(counselorFilter));
        }
    }

    public fetchCounselorFilter(): CounselorFilter {
        return this.storageService.getItem(StorageConstant.counselorFilter);
    }
}