import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AppRoutes } from '../constants/app-routes';
import { UserRoleConstant } from '../constants/user-role.constant';
import { LocalOpsService } from './local-ops.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private readonly localOpsService: LocalOpsService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | Promise<boolean> {

    const isOpenRoute = AppRoutes.openRoutes.indexOf(this.getRoutePath(state.url)) != -1;
    
    if (isOpenRoute) {
      return true;
    }

    const hasAccess = this.isRoleHaveRouteAccess(this.getRoutePath(state.url));

    if(!hasAccess) {
      this.router.navigateByUrl("login");
    }

    return true;
  }

  private isRoleHaveRouteAccess(routeUrl: string): boolean {
    
    const userData = this.localOpsService.fetchClaimDetailsFromAccessToken();

    switch(userData.role) {
      case UserRoleConstant.admin:
        return AppRoutes.adminRoutes.indexOf(routeUrl) != -1;
        
      case UserRoleConstant.therapist:
      case UserRoleConstant.counselor:
        return AppRoutes.therapistRoutes.indexOf(routeUrl) != -1;
        
      case UserRoleConstant.client:
        return AppRoutes.clientRoutes.indexOf(routeUrl) != -1;
      
      default:
        return AppRoutes.openRoutes.indexOf(routeUrl) != -1;

    }
  }

  private getRoutePath(url: string) {
    const path = url.split("?")[0];

    return path;
  }
}
