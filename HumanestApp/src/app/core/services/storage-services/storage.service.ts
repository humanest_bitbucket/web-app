import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  getItem = (_key: string): any => { };
  setItem = (_key: string, _value: string) => { };
  removeItem = (_key: string) => { };
  clear = () => { };
}

