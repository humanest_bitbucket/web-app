import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable()
export class BrowserStorageService implements StorageService {

  constructor() { }

  public getItem(key: string): any {
    let storedItem = window.localStorage.getItem(key);
    try {
      if(storedItem) {
        return JSON.parse(storedItem);
      }
      return null;
    } catch (ex) {
      return storedItem;
    }
  }

  setItem(key: string, value: any) {
    // We need to try and stringify it first (we can't save Objects/etc or it'll error out)
    if (typeof value !== 'string') {
      window.localStorage.setItem(key, JSON.stringify(value));
    } else {
      window.localStorage.setItem(key, value);
    }
  }

  removeItem(key: string) {
    window.localStorage.removeItem(key);
  }

  clear() {
    window.localStorage.clear();
  }
}
