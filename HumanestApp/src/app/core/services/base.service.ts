import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from './storage-services/storage.service';
import { HttpMethodType } from '../enums/http-method-type.enum';
import { CustomHeader } from '../models/custom-header';
import { environment } from '../../../environments/environment';
import { DateTimeHelper } from '../helpers/date-time.helper';
import { asyncScheduler, BehaviorSubject, Observable, scheduled, throwError, timer } from 'rxjs';
import { scan, delay, retryWhen, switchMap, map } from 'rxjs/operators';
import { StorageConstant } from '../constants/storage.constant';
import { HeaderConstant } from '../constants/header.constant';
import { HttpError } from '../models/http-error';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class BaseService {

  private _httpStatusCodeForBadRequest: number = 400;
  private _httpStatusCodeForUnauthorized: number = 401;
  private _httpStatusCodeForForbidden: number = 403;
  private _httpStatusCodeForNotFound: number = 404;
  private _httpStatusCodeForConflict: number = 409;
  private _httpStatusCodeForInternalServerError: number = 500;

  public static refreshingToken: boolean = false;
  //since 3rd party library needs access token and has to refresh token in there instances so we use subject for the same.    
  public static accessTokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(public httpClient: HttpClient
    , public storageService: StorageService
    , public location: Location
    , public router: Router
    , public toastrService: ToastrService) { }

  public getClaimsFromToken(): any {
    let claims: any = null;
    let token: string = this.storageService.getItem(StorageConstant.accessToken);
    if (token) {
      let tokenPayload: string = token.split('.')[1];
      let output: string = tokenPayload.replace(/-/g, '+').replace(/_/g, '/');
      switch (output.length % 4) {
        case 0:
          break;
        case 2:
          output += '==';
          break;
        case 3:
          output += '=';
          break;
        default:
          throw 'Illegal base64url string!';
      }
      claims = JSON.parse(atob(output));
    }

    return claims;
  }

  private createQueryString(params: any, parentPropertyName?: string): string {
    let queryString: string = '';
    if (params) {
      for (let property in params) {
        if (typeof params[property] === 'object')
          queryString += this.createQueryString(params[property], property);
        else if (params[property]) {
          if (queryString && queryString.length > 1) {
            queryString += '&';
          }

          queryString += (parentPropertyName ? parentPropertyName + '.' + property : property) + '=' + params[property];
        }
      }
    }
    return queryString;
  }

  private getRequestOptions(skipAuthentication: boolean = false, customHeaders?: CustomHeader[], contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text'): any {

    let requestOption: any = {};

    let requestHeaders: HttpHeaders = new HttpHeaders();

    if (!skipAuthentication) {
      let token: string = this.storageService.getItem(StorageConstant.accessToken);
      if (token) {
        requestHeaders = requestHeaders.append('Authorization', `Bearer ${token}`);
      }
    }

    if (contentType) {
      requestHeaders = requestHeaders.append('Content-Type', contentType);
    }
    else {
      requestHeaders = requestHeaders.append('Content-Type', 'application/json');
    }

    if (customHeaders && customHeaders.length > 0) {
      customHeaders.forEach(header => {
        requestHeaders = requestHeaders.append(header.key, header.value);
      });
    }

    requestOption.headers = requestHeaders;

    if (responseType) {
      requestOption.responseType = responseType;
    }
    else {
      requestOption.responseType = 'json' as const;
    }

    return requestOption;
  }

  private retry(error: Observable<any>): any {
    //retry API call 3 times with delay of 2 seconds
    return error.pipe(scan<any>((errorCount, err) => {
      let errorObject;
      if (err._body)
        errorObject = err.json();

      if (errorCount >= 3 || (errorObject && errorObject.message && errorObject.message.length > 0)) {
        throw err;
      }
      return errorCount + 1;
    }, 0), delay(2000));
  }

  private handleError(error: Response | any): Promise<any> {
    //We are not rejecting promise since if we reject it results into exception which in turn unsubscribe effect
    let message: string[] = [];
    let status: number;
    if (error.status != this._httpStatusCodeForUnauthorized && error.status != this._httpStatusCodeForForbidden && error.status != this._httpStatusCodeForNotFound && error.status != this._httpStatusCodeForInternalServerError) {
      if(error.error) {
        message = error.error;
      }
    }

    status = error.status;
    var httpError = new HttpError(message, status);
    return Promise.resolve(httpError);
  }

  private refreshAuthToken(url: string, httpMethodType: HttpMethodType, queryParams: any = null, requestBody: any = null, customHeaders?: CustomHeader[], skipAuthentication: boolean = false, contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text', ...pathParams: string[]) {
    let claims = this.getClaimsFromToken();
    let refreshTokenReqHeaders: CustomHeader[] = [];
    refreshTokenReqHeaders.push(new CustomHeader(HeaderConstant.refreshToken, claims.refreshToken));
    refreshTokenReqHeaders.push(new CustomHeader(HeaderConstant.token, this.storageService.getItem(StorageConstant.accessToken)));

    let options = this.getRequestOptions(true, refreshTokenReqHeaders, contentType, responseType);

    let refreshTokenUrl = environment.refreshTokenEndpoint;

    return this.httpClient
      .get<string>(`${environment.apiUrl}/${refreshTokenUrl}`, { headers: options.headers, responseType: 'json' as const })
      .pipe(
        retryWhen(this.retry)
        , switchMap((response) => {
          //once we receive token result, save it in session storage so other parallerl API calls can continue
          if (response) {
            this.storageService.setItem(StorageConstant.accessToken, response);

            BaseService.refreshingToken = false;
            BaseService.accessTokenSubject.next(response);

            //call to actual request the client made before refresh token.
            if (url) {
              switch (httpMethodType) {
                case HttpMethodType.Get:
                  this.get(url, queryParams, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
                  break;
                case HttpMethodType.Post:
                  this.post(url, queryParams, requestBody, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
                  break;
                case HttpMethodType.Put:
                  this.put(url, queryParams, requestBody, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
                  break;
                case HttpMethodType.Delete:
                  this.delete(url, queryParams, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
                  break;
              }
            }
            else {
              return scheduled(response, asyncScheduler);
            }
          }

          BaseService.refreshingToken = false;
          return throwError('Sorry! Your session has exipered. We are logging you out. Please re-login to continue using our services.');
        })
        , map(response => response));
      // .toPromise()
      // .catch(error => {
      //   BaseService.refreshingToken = false;
      //   return this.handleError(error);
      // });
  }

  private request(url: string, httpMethodType: HttpMethodType, queryParams: any = null, requestBody: any = null, customHeaders?: CustomHeader[], skipAuthentication: boolean = false, contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text', ...pathParams: string[]) {
    if (!skipAuthentication && this.isTokenExpired() && !BaseService.refreshingToken) {
      BaseService.refreshingToken = true;
      return this.refreshAuthToken(url, httpMethodType, queryParams, requestBody, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
    }
    else {
      let response: Observable<any>;
      if (BaseService.refreshingToken) {
        response = timer(2000)
          .pipe(switchMap(() => this.getHttpRequest(url, httpMethodType, queryParams, requestBody, customHeaders, skipAuthentication, contentType, responseType, ...pathParams)));
      }
      else {
        response = this.getHttpRequest(url, httpMethodType, queryParams, requestBody, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
      }

      return response;
      // return response
      //   .toPromise()
      //   .catch(this.handleError);
    }
  }

  private getHttpRequest(url: string, httpMethodType: HttpMethodType, queryParams: any = null, requestBody: any = null, customHeaders?: CustomHeader[], skipAuthentication: boolean = false, contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text', ...pathParams: string[]): Observable<any> {

    if (pathParams && pathParams.length > 0) {
      pathParams.forEach(param => {
        url += `/${param}`;
      });
    }

    if (queryParams) {
      let queryString = this.createQueryString(queryParams);
      if (queryString) {
        url += '?' + queryString;
      }
    }

    let requestOptions = this.getRequestOptions(skipAuthentication, customHeaders, contentType, responseType);
    let httpRequest;
    switch (httpMethodType) {
      case HttpMethodType.Get:
        httpRequest = this.httpClient
          .get(`${environment.apiUrl}/${url}`, requestOptions);
        break;
      case HttpMethodType.Post:
        httpRequest = this.httpClient
          .post(`${environment.apiUrl}/${url}`, requestBody, requestOptions);
        break;
      case HttpMethodType.Put:
        httpRequest = this.httpClient
          .put(`${environment.apiUrl}/${url}`, requestBody, requestOptions);
        break;
      case HttpMethodType.Delete:
        httpRequest = this.httpClient
          .delete(`${environment.apiUrl}/${url}`, requestOptions);
        break;
    }

    return scheduled(httpRequest, asyncScheduler);
  }

  public isTokenExpired(): boolean {
    var claims = this.getClaimsFromToken();
    if (claims && claims.exp) {
      let expires = DateTimeHelper.toLocalString(claims.exp * 1000); //multiplying with 1000 to convert to millisencond to get proper date
      if (expires) {
        var expiryDate = new Date(expires);
        var now = new Date();
        if (expiryDate > now)
          return false;
      }
    }
    return true;
  }

  public get(url: string, queryParams: any = null, customHeaders?: CustomHeader[], skipAuthentication: boolean = false, contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text', ...pathParams: string[]) {
    return this.request(url, HttpMethodType.Get, queryParams, null, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
  }

  public post(url: string, requestBody: any, queryParams: any = null, customHeaders?: CustomHeader[], skipAuthentication: boolean = false, contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text', ...pathParams: string[]) {
    return this.request(url, HttpMethodType.Post, queryParams, requestBody, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
  }

  public put(url: string, requestBody: any, queryParams: any = null, customHeaders?: CustomHeader[], skipAuthentication: boolean = false, contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text', ...pathParams: string[]) {
    return this.request(url, HttpMethodType.Put, queryParams, requestBody, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
  }

  public delete(url: string, queryParams: any = null, customHeaders?: CustomHeader[], skipAuthentication: boolean = false, contentType?: string, responseType?: 'arraybuffer' | 'blob' | 'json' | 'text', ...pathParams: string[]) {
    return this.request(url, HttpMethodType.Delete, queryParams, null, customHeaders, skipAuthentication, contentType, responseType, ...pathParams);
  }
}
