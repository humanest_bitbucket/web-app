export type GroupEvent = {
    description: string;
    id: number;
    name: string;
    type: number;
    timeZone: string;
    therapistId: string;
    startDateTime: string;
    endDateTime: string;
    duration: string | number;
    displayStart: string;
}