export interface ClientDataResponse {
    data: ClientData[];
    success: boolean;
}

interface ClientData{
    id: string;
    firstName: string;
    lastName: string;
    dob: Date;
    email: string;
}