export class CounselorFilter {
    specialty?: number[];
    language?: number[];
    dayOfWeek?: number[];
    licenseType?: number;
    pronouns?: number;
    state?: string;
}