export interface APIResponse {
    accessToken: string;
    success?: boolean;
    id?: string;
}