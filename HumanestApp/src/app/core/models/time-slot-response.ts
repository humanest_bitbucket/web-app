export interface TimeSlotResponse {
    data: TimeSlotData[];
}

interface TimeSlotData{
    status: string;
    isAvailable: boolean;
    dateTime: string;
    displayTime: string;
}

export interface ClientTimeSlotData{
    data: SlotData[];
    success?: boolean;
}

export interface SlotData{
    flag: string;
    day: string;
    date: Date;
    displayDate: string;
    appointmentSlots: TimeSlotData[];
}