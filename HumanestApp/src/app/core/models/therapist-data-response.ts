export interface TherapistDataResponse {
    data: TherapistData[];
    success: boolean;
}

interface TherapistData{
    id: string;
    email: string;
    fullName: string;
    license: string;
    bio: string;
    videoUrl: string;
}

export interface TherapistDetailsResponse {
    data: TherapistData;
    success: boolean;
}