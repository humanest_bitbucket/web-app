export class AccessToken {
    username?: string;
    role?: string;
    email?: string;
    userId?: string;
    refreshToken?: string;
    fullName?: string;
    firstName?: string;
    lastName?: string;
    isEmailConfirmed?: boolean;
    isProfileUpdated?: boolean;
    isPasswordUpdated?: boolean;
    isOTPValidated?: boolean;
}