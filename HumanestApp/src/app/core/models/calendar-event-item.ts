export class CalendarEventItem {
    public dateTime: Date;
    public description: string;
    public eventId: number;
    public eventType: number;
    public isAttendee: boolean;
    public name: string;
    public therapistName: string;
}