export function checkOverflow (element: HTMLElement) {
    return element.offsetHeight < element.scrollHeight ||
        element.offsetWidth < element.scrollWidth;
}

export function isMobileView() {
    return document.body.offsetWidth < 500;
}