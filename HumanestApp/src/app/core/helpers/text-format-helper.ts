import { EventType, EventTypeConstant } from "../enums/event-type.enum";

export function eventTypeText (type: number, name: string) {
    switch (type) {
        case EventType.TherapySession1on1:
            return EventTypeConstant.TherapySession1on1 + name;
        default:
            return "";
    }
}