import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserRoleConstant } from "../constants/user-role.constant";
import { AccessToken } from "../models/access-token";
import { LocalOpsService } from "../services/local-ops.service";

@Component({
    selector: 'base-comp',
    template: ''
  })
export class MustLoginBaseComponent {
    private baseUserData;

    constructor(private readonly baseLocalOpsService: LocalOpsService
        , private readonly baseRouter: Router) {
            this.baseUserData = this.baseLocalOpsService.fetchClaimDetailsFromAccessToken();
    }

    public validateLogin(): void {
        if(!this.baseUserData?.userId) {
            this.baseRouter.navigateByUrl("/login");
        }
    }

    public get isAdmin() {
        return this.baseUserData.role == UserRoleConstant.admin;
    }
    
    public get isMHW() {
        return this.baseUserData.role == UserRoleConstant.therapist || 
            this.baseUserData.role == UserRoleConstant.counselor;
    }
    
    public get isTherapist() {
        return this.baseUserData.role == UserRoleConstant.therapist;
    }
    
    public get isClient() {
        return this.baseUserData.role == UserRoleConstant.client;
    }

    public get UserData(): AccessToken {
        return this.baseLocalOpsService.fetchClaimDetailsFromAccessToken();
    }
}