import * as moment from 'moment/moment'

export class DateTimeHelper {
    private static toUtc(datetime: any): moment.Moment {
        return moment(datetime).utc();
    }

    private static toLocal(datetime: any): moment.Moment {
        return moment(datetime);
    }

    public static toLocalString(datetime: any, isDefault?: boolean, format?: string): string {
        if (format)
            return DateTimeHelper.toLocal(datetime).format(format);
        else
            if (isDefault)
                return DateTimeHelper.toLocal(datetime).format('MMM DD, YYYY');
            else
                return DateTimeHelper.toLocal(datetime).format();//ISO-8601
    }

    public static toUtcString(datetime: any, format?: string): any {
        if (format)
            return DateTimeHelper.toUtc(datetime).format(format);
        else
            return DateTimeHelper.toUtc(datetime).format();//ISO-8601
    }

    public static getDayDifference(firstdate: Date, secondDate: Date): number {
        return moment(firstdate).diff(secondDate, 'days');
    }
}

export function convertTimeSlotsByTimeZone(timezone: string, combinedSlots: string[]) {
    const convertedSlots: {convertedDate: string, utcDate: string}[] = [];

    combinedSlots.forEach(slotData => {
        var dec = moment(slotData);
        convertedSlots.push({
            convertedDate: dec.tz(timezone).format(),
            utcDate: slotData
        });
    });
    
    var allDatesArray = convertedSlots.map(x => x.convertedDate.substring(0, 10));
    var uniqueDates = allDatesArray.filter(onlyUnique);

    const result = [];
    uniqueDates.forEach((x) => {
        const date = x + "T00:00:00";
        var dateFormatted =  moment(date).format('DD-MM-YYYY');
        var weekDayName =  moment(date).format('dddd');
        var dayFlag = getDayFlag(date);
        const dateObj = { date: dateFormatted, flag: dayFlag, day: weekDayName, appointmentSlots: [],
         utcDate: x};
        dateObj.appointmentSlots = convertedSlots.filter(y => y.convertedDate.substring(0, 10) == x).map(z => {
            return { time: moment(z.convertedDate).format("hh:mm A"), id:z, utcDate: z.utcDate } 
        });

        result.push(dateObj);
    })

    return result;
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function getDayFlag(date) {
    var today = moment();
    var tomorrow = moment(today).add(1, 'day');    

    if(moment(today).isSame(date, 'day')) {
        return "Today";
    } else if(moment(tomorrow).isSame(date, 'day')) {
        return "Tomorrow";
    } else {
        var isSameWeek = moment(date).isSame(today, 'week');
        if(isSameWeek) {
            return "This Week";
        } else {
            return "Next Week"; // Since there will be maximum upcoming 5 days available slots
        }
    }
}

export function getStartDateOfWeek(date, weeksTobeSubstracted=0) {
    return moment(date).subtract(weeksTobeSubstracted, 'weeks').startOf('week').toDate();
}

export function getStartDateOfMonth(date, monthsTobeSubstracted=0) {
    return moment(date).subtract(monthsTobeSubstracted, 'months').startOf('month').toDate();
}