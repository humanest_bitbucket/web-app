import { Directive, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import {Loader, LoaderOptions} from 'google-maps';

@Directive({
  selector: '[googlePlace]'
})

export class GooglePlaceDirective {

  @Input("googlePlace") placeTypes: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  autocomplete: any;
  public _el: HTMLElement;

  constructor(public el: ElementRef) {
      this._el = el.nativeElement;        
  }

  async ngOnInit() {
    const options: LoaderOptions = {libraries: ["places"]};
    const loader = new Loader('AIzaSyB13N7g4bo4tjwid4TIYBHf3KqC8WF1J0U', options);
     
    const google = await loader.load();
    
      var input = this._el as any;
      //(cities)
      let autoCompleteOptions = {};
      if (this.placeTypes)
          autoCompleteOptions = { types: [this.placeTypes] }

      this.autocomplete = new google.maps.places.Autocomplete(input, autoCompleteOptions);
      google.maps.event.addListener(this.autocomplete, 'place_changed', () => {
          var place = this.autocomplete.getPlace();
          this.invokeEvent(place);
      });
  }

  invokeEvent(place: Object) {
      this.setAddress.emit(place);
  }

}
