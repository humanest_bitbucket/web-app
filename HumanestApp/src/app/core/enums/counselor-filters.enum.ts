import { Days } from "./day-list.enum";

export enum Pronouns {
    HeHim = 1
    , SheHer = 2
    , TheyThem = 3
}

export class PronounsList {
    public static pronounsList: Array<ListType> = [
        { id: Pronouns.HeHim, text: 'He/Him'}
        , { id: Pronouns.SheHer, text: 'She/Her'}
        , { id: Pronouns.TheyThem, text: 'They/Them'}
    ];
}

export enum LicenseType {
    LPCC = 1
    , LMFT = 2
    , LCSW = 3
    , ASCW = 4
    , APCC = 5
    , AMFT = 6
    , Psychotherapist = 7
    , MHW = 8
}

export class LicenseTypeList {
    public static licenseTypesList: Array<ListType> = [
        { id: LicenseType.LPCC, text: 'LPCC'}
        , { id: LicenseType.LMFT, text: 'LMFT'}
        , { id: LicenseType.LCSW, text: 'LCSW'}
        , { id: LicenseType.ASCW, text: 'ASCW'}
        , { id: LicenseType.APCC, text: 'APCC'}
        , { id: LicenseType.AMFT, text: 'AMFT'}
        , { id: LicenseType.Psychotherapist, text: 'Psychotherapist'}
        , { id: LicenseType.MHW, text: 'Mental Health Worker'}
    ];
}

export enum SpecialtyType {
    Acculturation  = 1
    , AlcoholDrugs = 2
    , AcademicJobPerformance = 3
    , ADHD = 4
    , Anger = 5
    , Anxiety = 6
    , Burnout = 7
    , Depression = 9
    , DomesticViolence = 10
    , EatingIssues = 11
    , FamilyOfOriginIssues = 12
    , Grief = 13
    , Identity = 14
    , LGBTQIA = 15
    , LifeTransitions = 16
    , Motivation = 17
    , Neurodivergence = 18
    , RaceIssues = 19
    , Relationships = 20
    , SexualAssault = 21
    , SelfEsteem = 22
    , Sleep = 23
    , Stress = 24
    , Trauma = 25
}

export class SpecialtyTypeList {
    public static specialtyList: Array<ListType> = [
        { id: SpecialtyType.Acculturation, text: 'Acculturation'}
        , { id: SpecialtyType.AlcoholDrugs, text: 'Alcohol or Drugs'}
        , { id: SpecialtyType.AcademicJobPerformance, text: 'Academic or Job Performance'}
        , { id: SpecialtyType.ADHD, text: 'ADHD'}
        , { id: SpecialtyType.Anger, text: 'Anger'}
        , { id: SpecialtyType.Anxiety, text: 'Anxiety'}
        , { id: SpecialtyType.Burnout, text: 'Burnout'}
        , { id: SpecialtyType.Depression, text: 'Depression'}
        , { id: SpecialtyType.DomesticViolence, text: 'Domestic Violence'}
        , { id: SpecialtyType.EatingIssues, text: 'Eating Issues'}
        , { id: SpecialtyType.FamilyOfOriginIssues, text: 'Family of Origin Issues'}
        , { id: SpecialtyType.Grief, text: 'Grief'}
        , { id: SpecialtyType.Identity, text: 'Identity'}
        , { id: SpecialtyType.LGBTQIA, text: 'LGBTQIA+'}
        , { id: SpecialtyType.LifeTransitions, text: 'Life Transitions'}
        , { id: SpecialtyType.Motivation, text: 'Motivation'}
        , { id: SpecialtyType.Neurodivergence, text: 'Neurodivergence'}
        , { id: SpecialtyType.RaceIssues, text: 'Race Issues'}
        , { id: SpecialtyType.Relationships, text: 'Relationships'}
        , { id: SpecialtyType.SexualAssault, text: 'Sexual Assault'}
        , { id: SpecialtyType.SelfEsteem, text: 'Self-Esteem'}
        , { id: SpecialtyType.Sleep, text: 'Sleep'}
        , { id: SpecialtyType.Stress, text: 'Stress'}
        , { id: SpecialtyType.Trauma, text: 'Trauma'}
    ];
}

export enum Languages {
    English = 1,
    Spanish = 2,
    German = 3
}

export class LanguagesList {
    public static languagesList: Array<ListType> = [
        { id: Languages.English, text: 'English'}
        , { id: Languages.Spanish, text: 'Spanish'}
        , { id: Languages.German, text: 'German'}
    ]
}

export class DaysList {
    public static daysList: Array<ListType> = [
        { id: Days.Sun, text: 'Sun'}
        , { id: Days.Mon, text: 'Mon'}
        , { id: Days.Tue, text: 'Tue'}
        , { id: Days.Wed, text: 'Wed'}
        , { id: Days.Thur, text: 'Thu'}
        , { id: Days.Fri, text: 'Fri'}
        , { id: Days.Sat, text:'Sat'}
    ]
}

export interface ListType{
    id: number;
    text: string;
}