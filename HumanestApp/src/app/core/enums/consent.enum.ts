export enum ConsentType {
    ConsentToTreatment = 1
    , PrivacyAgreement = 2
    , CrisisDisclaimer = 3
    , Over18Declaration = 4
}
