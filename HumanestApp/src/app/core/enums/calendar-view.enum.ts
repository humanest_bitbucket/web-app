export enum CalendarViews {
    TimeGridWeek = "timeGridWeek",
    DayGridMonth = "dayGridMonth",
    TimeGridDay = "timeGridDay",
    ListWeek = "listWeek"
}

export class CalendarViewsList {
    public static values = [
        { id: CalendarViews.TimeGridWeek, text: 'Week'}
        , { id: CalendarViews.DayGridMonth, text: 'Month'}
        , { id: CalendarViews.TimeGridDay, text: 'Day'}
        , { id: CalendarViews.ListWeek, text: 'List View'}
    ];
}