export enum RecurringType {
    EveryWeek = 1
    , Every2Weeks = 2
    , EveryMonth = 3
    , Custom = 4
}

export class RecurringTypeMapping {
    public static recurringTypes: Array<{ id: number, text: string }> = [
        { id: RecurringType.EveryWeek, text: 'Every Week' }
        , { id: RecurringType.Every2Weeks, text: 'Every 2 Weeks' }
        , { id: RecurringType.EveryMonth, text: 'Every Month' }
        , { id: RecurringType.Custom, text: 'Custom' }];
}

export enum RepeatEveryType {
    EveryDay = 1
    , EveryWeek = 2
    , EveryMonth = 3
    , EveryYear = 4
}

export class RepeatEveryTypeMapping {
    public static repeatEveryTypes: Array<{ id: number, text: string}> = [
        { id: RepeatEveryType.EveryDay, text: 'day'}
        , { id: RepeatEveryType.EveryWeek, text: 'week'}
        , { id: RepeatEveryType.EveryMonth, text: 'month'}
        , { id: RepeatEveryType.EveryYear, text: 'year'}];
}