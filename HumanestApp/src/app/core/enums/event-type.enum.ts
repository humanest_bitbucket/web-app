export enum EventType {
    TherapySession1on1 = 1
    , GroupEvent = 2
    , Workshop = 3
    , Course = 4
}

export class EventTypeMapping{
    public static eventTypes: Array<{ id: number, text: string }> = [
        { id: EventType.TherapySession1on1, text: '1:1 Therapy Appointment' }
        , { id: EventType.GroupEvent, text: 'Group Event' }
        , { id: EventType.Workshop, text: 'Workshop' }
        , { id: EventType.Course, text: 'Course' }];
}

export class EventTypeConstant {
    public static TherapySession1on1: string = '1:1 Therapy Appointment with ';
    public static TherapySession1on1Admin: string = '1:1 Therapy Appointment between {{therapist}} and {{client}} ';
}