export enum Days {
    Sun
    , Mon
    , Tue
    , Wed
    , Thur
    , Fri
    , Sat
}

export class DaysList {
    public static daysList: Array<DaysListType> = [
        { id: Days.Sun, text: 'S', shortText: 'Sun', fullText: 'Sunday', active: false }
        , { id: Days.Mon, text: 'M', shortText: 'Mon', fullText: 'Monday',active: false }
        , { id: Days.Tue, text: 'T', shortText: 'Tue', fullText: 'Tuesday',active: false }
        , { id: Days.Wed, text: 'W', shortText: 'Wed', fullText: 'Wednesday',active: false }
        , { id: Days.Thur, text: 'T', shortText: 'Thu', fullText: 'Thursday',active: false }
        , { id: Days.Fri, text: 'F', shortText: 'Fri', fullText: 'Friday',active: false }
        , { id: Days.Sat, text: 'S', shortText: 'Sat', fullText: 'Saturday',active: false }
    ];
}

export interface DaysListType{
    id: number;
    text: string;
    shortText: string;
    fullText: string;
    active: boolean
}