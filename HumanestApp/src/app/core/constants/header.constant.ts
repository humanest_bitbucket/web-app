export class HeaderConstant {
    public static refreshToken: string = 'refreshToken';
    public static token: string = 'token';
}
