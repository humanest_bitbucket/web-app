export class UserRoleConstant {
    public static readonly admin: string = 'Admin';
    public static readonly therapist: string = 'Therapist';
    public static readonly client: string = 'Client';
    public static readonly counselor: string = 'Counselor';
}
