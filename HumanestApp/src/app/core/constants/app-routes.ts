export class AppRoutes {
    public static openRoutes = [
        "/register",
        "/home",
        "/login",
        "/resetpassword",
        "/otp",
        "/emailvalidation",
        "/magiclink",
        "/updatepassword",
        "/404"
    ];

    public static adminRoutes = [
        "/admin/register",
        "/admin/event",
        "/admin/calendar",
        "/admin/pendingevents",
        "/admin/view-notes"
    ];

    public static therapistRoutes = [
        "/therapist/updateprofile",
        "/therapist/event",
        "/therapist/calendar",
        "/therapist/pendingevents",
        "/therapist/add-notes",
        "/therapist/view-notes",
        "/therapist/availability-slots"
    ];

    public static clientRoutes = [
        "/client/dashboard",
        "/client/initialprofile",
        "/client/schedule1on1",
        "/client/calendar",
        "/client/schedulesession",
        "/client/therapistdetails",
        "/client/intakeform",
        "/client/timeslot",
        "/client/confirmation",
        "/client/searchcounselor",
        "/client/counselorlist",
        "/client/groupevents"
    ];
}