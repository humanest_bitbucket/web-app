export class CalendarConstant {
    public static monthlyDate: string = 'Monthly on day';
    public static monthlyWeekDay: string = 'Monthly in the';
    public static noofWeek: string[] = ['first','second','third','fourth','last'];
}
