export class GenderConstant {
    public static genders: Array<{ id: string, text: string }> = [
        { id: 'Male', text: 'Male' }
        , { id: 'Female', text: 'Female' }
        , { id: 'Other', text: 'Other' }
        , { id: 'NoSay', text: 'Prefer not to say' }
    ];
}
