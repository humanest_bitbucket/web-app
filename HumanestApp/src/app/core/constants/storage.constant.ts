export class StorageConstant {    
    public static accessToken: string = "accessToken";
    public static counselorFilter: string = "counselorFilter";
    public static mhw: string = "mhw";
}
