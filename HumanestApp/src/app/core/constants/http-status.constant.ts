export class HttpStatusConstant {
    public static badRequest: number = 400;
    public static unauthorised: number = 401;
    public static forbidden: number = 403;
    public static notFound: number = 404;
    public static conflict: number = 409;
    public static internalServerError: number = 500;
}
