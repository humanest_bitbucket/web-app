import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpStatusConstant } from '../constants/http-status.constant';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class GlobalHttpInterceptor implements HttpInterceptor {

  constructor(private readonly router: Router
    , private readonly toastrService: ToastrService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse) {
          if (error.error instanceof ErrorEvent) {
            console.error("Unexpected error response recieved. Please contact adminstrator");
          } else {
            switch (error.status) {
              case HttpStatusConstant.unauthorised:
                //TODO: Logout functionality
                this.router.navigate(['/login']);
                break;
              case HttpStatusConstant.internalServerError:
                this.toastrService.error('Sorry! Something did not work as expected. Please refresh and try again.', 'Server Error!');
                break;
            }
          }
        } else {
          console.error("Unexpected response recieved from server. Please contact adminstrator.");
        }

        return throwError(error);
      })
    )
  }
}
