import { ElementRef, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public loaderElem: any;
  public loaderCount = 0; 

  constructor() { }

  setLoaderElement(loaderElement: ElementRef) {
    this.loaderElem = loaderElement.nativeElement;
  }

  show() {
    this.loaderCount++;
    this.loaderElem.style.display = "block";
  }

  hide() {
    this.loaderCount--;
    this.loaderElem.style.display = "none";
  }
  
}
