import { Component, ViewChild } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  @ViewChild("loader") loaderElem: any;
  title = 'humanestApp';
  
  constructor(private loaderService: LoaderService) {

  }

  ngAfterViewInit() {
    this.loaderService.setLoaderElement(this.loaderElem);  
  }
}
