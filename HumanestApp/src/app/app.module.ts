import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseService } from './core/services/base.service';
import { BrowserStorageService } from './core/services/storage-services/browser-storage.service';
import { StorageService } from './core/services/storage-services/storage.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GlobalHttpInterceptor } from './core/interceptors/global-http.interceptor';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AuthGuardService } from './core/services/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    ToastrModule.forRoot()    
  ],
  providers: [{
    provide: StorageService
    , useClass: BrowserStorageService
  }
  , {
     provide: HTTP_INTERCEPTORS
     , useClass: GlobalHttpInterceptor
     , multi: true  
  }
, BaseService
, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
